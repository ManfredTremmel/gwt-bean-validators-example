# gwt-bean-validators-example
Little example how to use bean validators in a GWT application (client and server side validation).
See live Demo on [https://test.m-share.de/validationexample/](https://test.m-share.de/validationexample/)

To run it local, you need **JDK >= 17** and **Maven >= 3.0.5**.
Checkout or download sources, go into the directory and compile it with

```bash
mvn clean install
```

The next step is to use the Spring Boot maven plugin to run the project:

```bash
mvn spring-boot:run
```

or start the generated executable jar file, it contains all you need to run the application:

```bash
java -jar server/target/gwt-bean-validators-example-server-2.4.1.jar 
```

Alternate you can build a docker container with the command:

 
```bash
mvn clean install
mvn spring-boot:build-image
```

and start it 

```bash
docker run -p 8080:8080 gwt-bean-validators-example-server:2.4.1
```

When the project is build successfully and running, you can access it at the URL : **[http://localhost:8080/validationexample/](http://localhost:8080/validationexample/)**

For login you can use the user/password combination test1/test1 or test2/test2, it's not secure, it's for demonstration only. The only secured pages are a dummy content page and a backoffice example page...
