/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.login;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.knightsoftnet.validationexample.client.ui.page.login.LoginViewGwtImpl.Binder;
import de.knightsoftnet.validationexample.client.ui.page.login.LoginViewGwtImpl.Driver;
import de.knightsoftnet.validationexample.gwtmockito.GwtMockitoExtension;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

/**
 * tests for login view.
 *
 * @author Manfred Tremmel
 */
@ExtendWith(GwtMockitoExtension.class)
public class LoginViewTest {

  @Mock
  Driver driver;
  @Mock
  Binder uiBinder;

  @Mock
  Widget uiBinderWidget;
  @Mock
  LoginPresenter loginPresenter;

  LoginViewGwtImpl loginView;

  /**
   * initialize date for tests.
   */
  @BeforeEach
  public void initMapping() {
    when(uiBinderWidget.asWidget()).thenReturn(uiBinderWidget);
    when(uiBinder.createAndBindUi(any())).thenReturn(uiBinderWidget);
    loginView = new LoginViewGwtImpl(driver, uiBinder);
    loginView.logMessages = mock(Label.class);
  }

  @Test
  public void onFormSubmitTest() {
    // given
    loginView.setPresenter(loginPresenter);

    // when
    loginView.onFormSubmit(null);

    // then
    verify(loginPresenter).tryToLogin();
  }

  @Test
  public void showMessageTest() {
    // given
    final String message = "test";

    // when
    loginView.showMessage(message);

    // then
    verify(loginView.logMessages).setText(eq(message));
  }
}
