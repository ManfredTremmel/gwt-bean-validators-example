package de.knightsoftnet.validationexample.client.ui.page.settings;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.knightsoftnet.validationexample.client.ui.page.settings.SettingsPresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.page.settings.SettingsPresenter.MyView;

import com.google.gwt.inject.client.binder.GinAnnotatedBindingBuilder;
import com.google.gwt.inject.client.binder.GinBinder;
import com.google.gwt.inject.client.binder.GinScopedBindingBuilder;
import com.google.inject.Singleton;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * tests for SettingsModule.
 *
 * @author Manfred Tremmel
 */
public class SettingsModuleTest {

  @Mock
  private GinBinder binder;
  @SuppressWarnings("rawtypes")
  @Mock
  private GinAnnotatedBindingBuilder annotationBuilder;
  @Mock
  private GinScopedBindingBuilder scopeBinder;

  /**
   * initialize date for tests.
   */
  @SuppressWarnings("unchecked")
  @BeforeEach
  public void initMapping() {
    MockitoAnnotations.openMocks(this);
    when(binder.bind(any(Class.class))).thenReturn(annotationBuilder);
    when(annotationBuilder.to(any(Class.class))).thenReturn(scopeBinder);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void bindTest() {
    // given
    final SettingsModule module = new SettingsModule();

    // when
    module.configure(binder);

    // then
    verify(binder).bind(SettingsPresenter.class);
    verify(binder).bind(MyView.class);
    verify(binder).bind(SettingsViewGwtImpl.class);
    verify(annotationBuilder).to(SettingsViewGwtImpl.class);
    verify(annotationBuilder, times(2)).in(Singleton.class);
    verify(binder).bind(MyProxy.class);
    verify(annotationBuilder).asEagerSingleton();
  }
}
