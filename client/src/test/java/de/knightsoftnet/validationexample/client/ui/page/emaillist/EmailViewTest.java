/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.emaillist;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import de.knightsoftnet.mtwidgets.client.ui.widget.TextBox;
import de.knightsoftnet.validationexample.client.ui.page.emaillist.EmailViewGwtImpl.Binder;
import de.knightsoftnet.validationexample.client.ui.page.emaillist.EmailViewGwtImpl.Driver;
import de.knightsoftnet.validationexample.gwtmockito.GwtMockitoExtension;
import de.knightsoftnet.validationexample.shared.models.EmailData;
import de.knightsoftnet.validationexample.shared.models.EmailListData;
import de.knightsoftnet.validationexample.shared.models.EmailTypeEnum;
import de.knightsoftnet.validators.client.editor.ValueBoxEditor;
import de.knightsoftnet.validators.client.editor.impl.ListValidationEditor;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import org.gwtproject.editor.client.EditorError;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;

import java.util.List;

import jakarta.inject.Provider;

/**
 * tests for Email view.
 *
 * @author Manfred Tremmel
 */
@ExtendWith(GwtMockitoExtension.class)
public class EmailViewTest {

  @Mock
  Binder uiBinder;

  Driver driver;

  @Mock
  Provider<EmailListEditor> emailListEditorProvider;

  @Mock
  Widget uiBinderWidget;
  @Mock
  EmailPresenter emailPresenter;

  @Mock
  TextBox firstname;
  @Mock
  TextBox lastname;
  @Mock
  EmailListEditor emailList;
  @Mock
  ListValidationEditor<EmailData, EmailItemViewImpl> emailListEditor;

  @Mock
  Label logMessages;
  @Mock
  Button addressButton;

  EmailViewGwtImpl emailView;

  /**
   * initialize date for tests.
   */
  @SuppressWarnings("unchecked")
  @BeforeEach
  public void initMapping() {
    when(uiBinderWidget.asWidget()).thenReturn(uiBinderWidget);
    when(firstname.asEditor()).thenReturn(ValueBoxEditor.of(firstname));
    doAnswer(
        answer -> when(firstname.getValueOrThrow()).thenReturn((String) answer.getArguments()[0]))
            .when(firstname).setValue(any());
    when(lastname.asEditor()).thenReturn(ValueBoxEditor.of(lastname));
    doAnswer(
        answer -> when(lastname.getValueOrThrow()).thenReturn((String) answer.getArguments()[0]))
            .when(lastname).setValue(any());
    when(emailList.asEditor()).thenReturn(emailListEditor);
    // when(emailList.createData()).thenCallRealMethod();
    doAnswer(answer -> when(emailListEditor.getList())
        .thenReturn((List<EmailData>) answer.getArguments()[0])).when(emailListEditor)
            .setValue(any());
    when(uiBinder.createAndBindUi(any())).thenAnswer(answer -> {
      final EmailViewGwtImpl emailView = answer.getArgument(0);
      emailView.firstname = firstname;
      emailView.lastname = lastname;
      emailView.emailList = emailList;
      emailView.logMessages = logMessages;
      emailView.addressButton = addressButton;

      return uiBinderWidget;
    });

    driver = new EmailViewGwtImpl_Driver_Impl();
    emailView = new EmailViewGwtImpl(driver, uiBinder, emailListEditorProvider);
  }

  @Test
  public void onFormSubmitTest() {
    // given
    emailView.setPresenter(emailPresenter);

    // when
    emailView.onFormSubmit(null);

    // then
    verify(emailPresenter).tryToSend();
  }

  @Test
  public void showMessageTest() {
    // given
    final String message = "test";

    // when
    emailView.showMessage(message);

    // then
    verify(emailView.logMessages).setText(eq(message));
  }

  @Test
  public void setAndValidateEmptyFormTest() {
    // given
    final EmailListData emailData = new EmailListData();

    // when
    emailView.fillForm(emailData);

    // then
    verify(emailView.firstname).setValue(eq(emailData.getFirstname()));
    verify(emailView.lastname).setValue(eq(emailData.getLastname()));
    verify(emailListEditor).setValue(eq(emailData.getEmailList()));

    verify(emailView.firstname, times(1))
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(emailView.lastname, times(1))
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(emailView.emailList, times(1))
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
  }

  @Test
  public void setAndValidateValidFormTest() {
    // given
    final EmailListData emailListData = new EmailListData();
    final EmailData emailData = new EmailData();
    emailData.setEmail("test@test.de");
    emailData.setType(EmailTypeEnum.HOME);
    emailListData.setFirstname("firstname");
    emailListData.setLastname("lastname");
    emailListData.setEmailList(List.of(emailData));

    // when
    emailView.fillForm(emailListData);

    // then
    verify(emailView.firstname).setValue(eq(emailListData.getFirstname()));
    verify(emailView.lastname).setValue(eq(emailListData.getLastname()));
    verify(emailListEditor).setValue(eq(emailListData.getEmailList()));

    verify(emailView.firstname, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(emailView.lastname, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(emailView.emailList, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
  }
}
