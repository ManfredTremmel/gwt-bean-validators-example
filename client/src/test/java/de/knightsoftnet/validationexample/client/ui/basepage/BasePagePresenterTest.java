/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.basepage;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.verify;

import de.knightsoftnet.validationexample.gwtmockito.GwtMockitoExtension;

import com.google.gwtmockito.GwtMock;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 * tests for BasePage Presenter.
 *
 * @author Manfred Tremmel
 */
@ExtendWith(GwtMockitoExtension.class)
public class BasePagePresenterTest {

  @GwtMock
  BasePagePresenter basePagePresenter;

  @Test
  public void onBindTest() {
    // given
    doCallRealMethod().when(basePagePresenter).onBind();

    // when
    basePagePresenter.onBind();

    // then
    verify(basePagePresenter).setInSlot(eq(BasePagePresenter.SLOT_NAVIGATION),
        eq(basePagePresenter.navigationPresenter));
  }

  @Test
  public void showInfoTest() {
    // given
    doCallRealMethod().when(basePagePresenter).showInfo();

    // when
    basePagePresenter.showInfo();

    // then
    verify(basePagePresenter).addToPopupSlot(eq(basePagePresenter.aboutPresenter));
  }
}
