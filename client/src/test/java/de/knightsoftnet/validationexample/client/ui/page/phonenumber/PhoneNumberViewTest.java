/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.phonenumber;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import de.knightsoftnet.mtwidgets.client.ui.widget.CountryListBox;
import de.knightsoftnet.mtwidgets.client.ui.widget.PhoneNumberMsSuggestBox;
import de.knightsoftnet.validationexample.client.ui.page.phonenumber.PhoneNumberViewGwtImpl.Binder;
import de.knightsoftnet.validationexample.client.ui.page.phonenumber.PhoneNumberViewGwtImpl.Driver;
import de.knightsoftnet.validationexample.gwtmockito.GwtMockitoExtension;
import de.knightsoftnet.validationexample.shared.models.PhoneNumberData;
import de.knightsoftnet.validators.client.decorators.ExtendedValueBoxEditor;
import de.knightsoftnet.validators.client.editor.TakesValueEditor;
import de.knightsoftnet.validators.shared.data.CountryEnum;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import org.gwtproject.editor.client.EditorError;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;

import java.util.List;

/**
 * tests for phone number view.
 *
 * @author Manfred Tremmel
 */
@ExtendWith(GwtMockitoExtension.class)
public class PhoneNumberViewTest {

  @Mock
  Binder uiBinder;

  Driver driver;

  @Mock
  Widget uiBinderWidget;
  @Mock
  PhoneNumberPresenter phoneNumberPresenter;

  @Mock
  CountryListBox countryCode;
  @Mock
  PhoneNumberMsSuggestBox phoneNumber;

  @Mock
  Label logMessages;
  @Mock
  Button phoneNumberButton;

  PhoneNumberViewGwtImpl phoneNumberView;

  /**
   * initialize date for tests.
   */
  @BeforeEach
  public void initMapping() {
    when(uiBinderWidget.asWidget()).thenReturn(uiBinderWidget);
    when(countryCode.asEditor()).thenReturn(new ExtendedValueBoxEditor<>(countryCode, null));
    doAnswer(
        answer -> when(countryCode.getValue()).thenReturn((CountryEnum) answer.getArguments()[0]))
            .when(countryCode).setValue(any());
    when(phoneNumber.asEditor()).thenReturn(TakesValueEditor.of(phoneNumber));
    doAnswer(answer -> when(phoneNumber.getValue()).thenReturn((String) answer.getArguments()[0]))
        .when(phoneNumber).setValue(any());
    when(uiBinder.createAndBindUi(any())).thenAnswer(answer -> {
      final PhoneNumberViewGwtImpl phoneNumberView = answer.getArgument(0);
      phoneNumberView.countryCode = countryCode;
      phoneNumberView.phoneNumber = phoneNumber;
      phoneNumberView.logMessages = logMessages;
      phoneNumberView.phoneNumberButton = phoneNumberButton;

      return uiBinderWidget;
    });

    driver = new PhoneNumberViewGwtImpl_Driver_Impl();
    phoneNumberView = new PhoneNumberViewGwtImpl(driver, uiBinder);
  }

  @Test
  public void onFormSubmitTest() {
    // given
    phoneNumberView.setPresenter(phoneNumberPresenter);

    // when
    phoneNumberView.onFormSubmit(null);

    // then
    verify(phoneNumberPresenter).tryToSend();
  }

  @Test
  public void showMessageTest() {
    // given
    final String message = "test";

    // when
    phoneNumberView.showMessage(message);

    // then
    verify(phoneNumberView.logMessages).setText(eq(message));
  }

  @Test
  public void setAndValidateEmptyFormTest() {
    // given
    final PhoneNumberData phoneNumberData = new PhoneNumberData();

    // when
    phoneNumberView.fillForm(phoneNumberData);

    // then
    verify(phoneNumberView.countryCode).setValue(eq(phoneNumberData.getCountryCode()));
    verify(phoneNumberView.phoneNumber).setValue(eq(phoneNumberData.getPhoneNumber()));

    verify(phoneNumberView.countryCode, times(1))
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(phoneNumberView.phoneNumber, times(1))
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
  }

  @Test
  public void setAndValidateValidCountryFormTest() {
    // given
    final PhoneNumberData phoneNumberData = new PhoneNumberData();
    phoneNumberData.setCountryCode(CountryEnum.DE);
    phoneNumberData.setPhoneNumber("+49 (30) 1234567");

    // when
    phoneNumberView.fillForm(phoneNumberData);

    // then
    verify(phoneNumberView.countryCode).setValue(eq(phoneNumberData.getCountryCode()));
    verify(phoneNumberView.phoneNumber).setValue(eq(phoneNumberData.getPhoneNumber()));

    verify(phoneNumberView.countryCode, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(phoneNumberView.phoneNumber, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
  }
}
