/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.basepage.about;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.knightsoftnet.validationexample.client.ui.basepage.about.AboutViewGwtImpl.Binder;
import de.knightsoftnet.validationexample.gwtmockito.GwtMockitoExtension;

import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

/**
 * tests for phone number view.
 *
 * @author Manfred Tremmel
 */
@ExtendWith(GwtMockitoExtension.class)
public class AboutViewTest {

  @Mock
  Binder uiBinder;
  @Mock
  Widget uiBinderWidget;
  @Mock
  EventBus eventBus;

  @Mock
  AboutPresenter aboutPresenter;

  AboutViewGwtImpl aboutView;

  /**
   * initialize date for tests.
   */
  @BeforeEach
  public void initMapping() {
    when(uiBinderWidget.asWidget()).thenReturn(uiBinderWidget);
    when(uiBinder.createAndBindUi(any())).thenReturn(uiBinderWidget);
    aboutView = new AboutViewGwtImpl(uiBinder, eventBus);
    aboutView.setPresenter(aboutPresenter);
  }

  @Test
  public void onClickTest() {
    // given

    // when
    aboutView.onClick(null);

    // then
    verify(aboutPresenter).removeFromParentSlot();
  }
}
