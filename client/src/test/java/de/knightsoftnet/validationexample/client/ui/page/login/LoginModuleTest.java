package de.knightsoftnet.validationexample.client.ui.page.login;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.knightsoftnet.validationexample.client.ui.page.login.LoginPresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.page.login.LoginPresenter.MyView;
import de.knightsoftnet.validationexample.client.ui.page.login.LoginViewGwtImpl.Driver;

import com.google.gwt.inject.client.binder.GinAnnotatedBindingBuilder;
import com.google.gwt.inject.client.binder.GinBinder;
import com.google.gwt.inject.client.binder.GinScopedBindingBuilder;
import com.google.inject.Singleton;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * tests for LoginModule.
 *
 * @author Manfred Tremmel
 */
public class LoginModuleTest {

  @Mock
  private GinBinder binder;
  @SuppressWarnings("rawtypes")
  @Mock
  private GinAnnotatedBindingBuilder annotationBuilder;
  @Mock
  private GinScopedBindingBuilder scopeBinder;

  /**
   * initialize date for tests.
   */
  @SuppressWarnings("unchecked")
  @BeforeEach
  public void initMapping() {
    MockitoAnnotations.openMocks(this);
    when(binder.bind(any(Class.class))).thenReturn(annotationBuilder);
    when(annotationBuilder.to(any(Class.class))).thenReturn(scopeBinder);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void bindTest() {
    // given
    final LoginModule module = new LoginModule();

    // when
    module.configure(binder);

    // then
    verify(binder).bind(LoginPresenter.class);
    verify(binder).bind(MyView.class);
    verify(binder).bind(LoginViewGwtImpl.class);
    verify(annotationBuilder).to(LoginViewGwtImpl.class);
    verify(annotationBuilder, times(2)).in(Singleton.class);
    verify(binder).bind(MyProxy.class);
    verify(annotationBuilder).asEagerSingleton();
    verify(binder).bind(Driver.class);
    verify(annotationBuilder).to(LoginViewGwtImpl_Driver_Impl.class);
  }
}
