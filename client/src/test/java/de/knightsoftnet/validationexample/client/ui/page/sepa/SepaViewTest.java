/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.sepa;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import de.knightsoftnet.mtwidgets.client.ui.widget.BicSuggestBox;
import de.knightsoftnet.mtwidgets.client.ui.widget.CountryListBox;
import de.knightsoftnet.mtwidgets.client.ui.widget.IbanTextBox;
import de.knightsoftnet.mtwidgets.client.ui.widget.TextBox;
import de.knightsoftnet.validationexample.client.ui.page.sepa.SepaViewGwtImpl.Binder;
import de.knightsoftnet.validationexample.client.ui.page.sepa.SepaViewGwtImpl.Driver;
import de.knightsoftnet.validationexample.gwtmockito.GwtMockitoExtension;
import de.knightsoftnet.validationexample.shared.models.SepaData;
import de.knightsoftnet.validators.client.decorators.ExtendedValueBoxEditor;
import de.knightsoftnet.validators.client.editor.TakesValueEditor;
import de.knightsoftnet.validators.client.editor.ValueBoxEditor;
import de.knightsoftnet.validators.shared.data.CountryEnum;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import org.gwtproject.editor.client.EditorError;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;

import java.util.List;

/**
 * tests for phone number view.
 *
 * @author Manfred Tremmel
 */
@ExtendWith(GwtMockitoExtension.class)
public class SepaViewTest {

  @Mock
  Binder uiBinder;

  Driver driver;

  @Mock
  Widget uiBinderWidget;
  @Mock
  SepaPresenter sepaPresenter;

  @Mock
  TextBox bankName;
  @Mock
  TextBox accountOwner;
  @Mock
  CountryListBox countryCode;
  @Mock
  IbanTextBox iban;
  @Mock
  BicSuggestBox bic;

  @Mock
  Label logMessages;
  @Mock
  Button sepaButton;

  SepaViewGwtImpl sepaView;

  /**
   * initialize date for tests.
   */
  @BeforeEach
  public void initMapping() {
    when(uiBinderWidget.asWidget()).thenReturn(uiBinderWidget);
    when(bankName.asEditor()).thenReturn(ValueBoxEditor.of(bankName));
    doAnswer(
        answer -> when(bankName.getValueOrThrow()).thenReturn((String) answer.getArguments()[0]))
            .when(bankName).setValue(any());
    when(accountOwner.asEditor()).thenReturn(ValueBoxEditor.of(accountOwner));
    doAnswer(answer -> when(accountOwner.getValueOrThrow())
        .thenReturn((String) answer.getArguments()[0])).when(accountOwner).setValue(any());
    when(countryCode.asEditor()).thenReturn(new ExtendedValueBoxEditor<>(countryCode, null));
    doAnswer(
        answer -> when(countryCode.getValue()).thenReturn((CountryEnum) answer.getArguments()[0]))
            .when(countryCode).setValue(any());
    when(iban.asEditor()).thenReturn(ValueBoxEditor.of(iban));
    doAnswer(answer -> when(iban.getValueOrThrow()).thenReturn((String) answer.getArguments()[0]))
        .when(iban).setValue(any());
    when(bic.asEditor()).thenReturn(TakesValueEditor.of(bic));
    doAnswer(answer -> when(bic.getValue()).thenReturn((String) answer.getArguments()[0])).when(bic)
        .setValue(any());
    when(uiBinder.createAndBindUi(any())).thenAnswer(answer -> {
      final SepaViewGwtImpl sepaView = answer.getArgument(0);
      sepaView.bankName = bankName;
      sepaView.accountOwner = accountOwner;
      sepaView.countryCode = countryCode;
      sepaView.iban = iban;
      sepaView.bic = bic;
      sepaView.logMessages = logMessages;
      sepaView.sepaButton = sepaButton;

      return uiBinderWidget;
    });

    driver = new SepaViewGwtImpl_Driver_Impl();
    sepaView = new SepaViewGwtImpl(driver, uiBinder);
  }

  @Test
  public void onFormSubmitTest() {
    // given
    sepaView.setPresenter(sepaPresenter);

    // when
    sepaView.onFormSubmit(null);

    // then
    verify(sepaPresenter).tryToSend();
  }

  @Test
  public void showMessageTest() {
    // given
    final String message = "test";

    // when
    sepaView.showMessage(message);

    // then
    verify(sepaView.logMessages).setText(eq(message));
  }

  @Test
  public void setAndValidateEmptyFormTest() {
    // given
    final SepaData sepaData = new SepaData();

    // when
    sepaView.fillForm(sepaData);

    // then
    verify(sepaView.bankName).setValue(eq(sepaData.getBankName()));
    verify(sepaView.accountOwner).setValue(eq(sepaData.getAccountOwner()));
    verify(sepaView.countryCode).setValue(eq(sepaData.getCountryCode()));
    verify(sepaView.iban).setValue(eq(sepaData.getIban()));
    verify(sepaView.bic).setValue(eq(sepaData.getBic()));

    verify(sepaView.bankName, times(1))
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(sepaView.accountOwner, times(1))
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(sepaView.countryCode, times(1))
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(sepaView.iban, times(1))
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(sepaView.bic, times(1))
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
  }

  @Test
  public void setAndValidateValidCountryFormTest() {
    // given
    final SepaData sepaData = new SepaData();
    sepaData.setBankName("Test Bank");
    sepaData.setAccountOwner("owner");
    sepaData.setCountryCode(CountryEnum.DE);
    sepaData.setIban("DE16 7016 0000 0000 5554 44");
    sepaData.setBic("GENODEFF701");

    // when
    sepaView.fillForm(sepaData);

    // then
    verify(sepaView.bankName).setValue(eq(sepaData.getBankName()));
    verify(sepaView.accountOwner).setValue(eq(sepaData.getAccountOwner()));
    verify(sepaView.countryCode).setValue(eq(sepaData.getCountryCode()));
    verify(sepaView.iban).setValue(eq(sepaData.getIban()));
    verify(sepaView.bic).setValue(eq(sepaData.getBic()));

    verify(sepaView.bankName, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(sepaView.accountOwner, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(sepaView.countryCode, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(sepaView.iban, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(sepaView.bic, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
  }
}
