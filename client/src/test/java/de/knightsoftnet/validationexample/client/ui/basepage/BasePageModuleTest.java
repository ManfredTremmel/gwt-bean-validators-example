package de.knightsoftnet.validationexample.client.ui.basepage;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.knightsoftnet.validationexample.client.ui.basepage.BasePagePresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.basepage.BasePagePresenter.MyView;

import com.google.gwt.inject.client.binder.GinAnnotatedBindingBuilder;
import com.google.gwt.inject.client.binder.GinBinder;
import com.google.gwt.inject.client.binder.GinScopedBindingBuilder;
import com.google.inject.Singleton;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * tests for BasePageModule.
 *
 * @author Manfred Tremmel
 */
public class BasePageModuleTest {

  @Mock
  private GinBinder binder;
  @SuppressWarnings("rawtypes")
  @Mock
  private GinAnnotatedBindingBuilder annotationBuilder;
  @Mock
  private GinScopedBindingBuilder scopeBinder;

  /**
   * initialize date for tests.
   */
  @SuppressWarnings("unchecked")
  @BeforeEach
  public void initMapping() {
    MockitoAnnotations.openMocks(this);
    when(binder.bind(any(Class.class))).thenReturn(annotationBuilder);
    when(annotationBuilder.to(any(Class.class))).thenReturn(scopeBinder);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void bindTest() {
    // given
    final BasePageModule module = new BasePageModule();

    // when
    module.configure(binder);

    // then
    verify(binder).bind(BasePagePresenter.class);
    verify(binder).bind(MyView.class);
    verify(binder).bind(BasePageViewGwtImpl.class);
    verify(annotationBuilder).to(BasePageViewGwtImpl.class);
    verify(annotationBuilder, times(2)).in(Singleton.class);
    verify(binder).bind(MyProxy.class);
    verify(annotationBuilder).asEagerSingleton();
  }
}
