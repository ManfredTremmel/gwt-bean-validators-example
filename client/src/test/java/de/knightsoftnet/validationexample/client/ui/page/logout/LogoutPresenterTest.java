/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.logout;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.knightsoftnet.gwtp.spring.client.rest.helper.HttpMessages;
import de.knightsoftnet.gwtp.spring.client.rest.helper.LoginMessages;
import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.validationexample.client.services.UserRestService;
import de.knightsoftnet.validationexample.client.ui.page.logout.LogoutPresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.page.logout.LogoutPresenter.MyView;
import de.knightsoftnet.validationexample.gwtmockito.GwtMockitoExtension;

import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rest.delegates.client.AbstractResourceDelegate;
import com.gwtplatform.dispatch.rest.delegates.test.DelegateTestUtils;
import com.gwtplatform.dispatch.shared.ActionException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

/**
 * tests for Logout Presenter.
 *
 * @author Manfred Tremmel
 */
@ExtendWith(GwtMockitoExtension.class)
public class LogoutPresenterTest {

  @Mock
  EventBus eventBus;
  @Mock
  MyView view;
  @Mock
  MyProxy proxy;
  @Mock
  AbstractResourceDelegate<UserRestService> userService;
  @Mock
  UserRestService userRestService;
  @Mock
  Session session;

  LogoutPresenter logoutPresenter;

  @Mock
  Response response;
  @Mock
  ActionException actionException;
  @Mock
  private LoginMessages messages;
  @Mock
  private HttpMessages httpMessages;

  /**
   * initialize date for tests.
   */
  @BeforeEach
  public void initMapping() {
    when(userService.withCallback(any(AsyncCallback.class))).thenReturn(userRestService);
    logoutPresenter = new LogoutPresenter(eventBus, view, proxy, userService, session);
  }

  @Test
  public void onRevealSuccessTest() {
    // given
    DelegateTestUtils.givenDelegate(userService).useResource(UserRestService.class).and().succeed()
        .withResult(null).when().logout();

    // when
    logoutPresenter.onReveal();

    // then
    verify(session).readSessionData();
  }

  @Test
  public void onRevealFailureTest() {
    // given
    DelegateTestUtils.givenDelegate(userService).useResource(UserRestService.class).and().fail()
        .when().logout();

    // when
    logoutPresenter.onReveal();

    // then
    verify(session).readSessionData();
  }
}
