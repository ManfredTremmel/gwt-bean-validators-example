/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.gin;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.knightsoftnet.gwtp.spring.client.gin.ServiceModule;
import de.knightsoftnet.gwtp.spring.client.resources.ResourceLoader;
import de.knightsoftnet.mtwidgets.client.ui.widget.styling.WidgetResourceLoader;

import com.google.gwt.inject.client.binder.GinAnnotatedBindingBuilder;
import com.google.gwt.inject.client.binder.GinBinder;
import com.google.gwt.inject.client.binder.GinScopedBindingBuilder;
import com.gwtplatform.mvp.client.gin.DefaultModule;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * tests for ClientModule.
 *
 * @author Manfred Tremmel
 */
public class ClientModuleTest {

  @Mock
  private GinBinder binder;
  @SuppressWarnings("rawtypes")
  @Mock
  private GinAnnotatedBindingBuilder annotationBuilder;
  @Mock
  private GinScopedBindingBuilder scopedBuilder;

  /**
   * initialize date for tests.
   */
  @SuppressWarnings("unchecked")
  @BeforeEach
  public void initMapping() {
    MockitoAnnotations.openMocks(this);
    when(binder.bind(ArgumentMatchers.any(Class.class))).thenReturn(annotationBuilder);
    when(annotationBuilder.to(ArgumentMatchers.any(Class.class))).thenReturn(scopedBuilder);
  }

  @Test
  public void bindTest() {
    // given
    final ClientModule module = new ClientModule();

    // when
    module.configure(binder);

    // then
    verify(binder, Mockito.times(3)).install(ArgumentMatchers.any());
    verify(binder).install(any(DefaultModule.class));
    verify(binder).install(any(ApplicationModule.class));
    verify(binder).install(any(ServiceModule.class));
    verify(binder).bind(ResourceLoader.class);
    verify(binder).bind(WidgetResourceLoader.class);
  }
}
