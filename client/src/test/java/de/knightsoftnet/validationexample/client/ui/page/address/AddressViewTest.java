/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.address;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import de.knightsoftnet.mtwidgets.client.ui.widget.CountryListBox;
import de.knightsoftnet.mtwidgets.client.ui.widget.PostalCodeTextBox;
import de.knightsoftnet.mtwidgets.client.ui.widget.TextBox;
import de.knightsoftnet.validationexample.client.ui.page.address.AddressViewGwtImpl.Binder;
import de.knightsoftnet.validationexample.client.ui.page.address.AddressViewGwtImpl.Driver;
import de.knightsoftnet.validationexample.gwtmockito.GwtMockitoExtension;
import de.knightsoftnet.validationexample.shared.models.PostalAddressData;
import de.knightsoftnet.validators.client.decorators.ExtendedValueBoxEditor;
import de.knightsoftnet.validators.client.editor.ValueBoxEditor;
import de.knightsoftnet.validators.shared.data.CountryEnum;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import org.gwtproject.editor.client.EditorError;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;

import java.util.List;

/**
 * tests for address view.
 *
 * @author Manfred Tremmel
 */
@ExtendWith(GwtMockitoExtension.class)
public class AddressViewTest {

  @Mock
  Binder uiBinder;

  Driver driver;

  @Mock
  Widget uiBinderWidget;
  @Mock
  AddressPresenter addressPresenter;

  @Mock
  TextBox postOfficeBox;
  @Mock
  TextBox street;
  @Mock
  TextBox streetNumber;
  @Mock
  TextBox streetNumberAdditional;
  @Mock
  TextBox extended;
  @Mock
  PostalCodeTextBox postalCode;
  @Mock
  TextBox locality;
  @Mock
  TextBox region;
  @Mock
  CountryListBox countryCode;

  @Mock
  Label logMessages;
  @Mock
  Button addressButton;

  AddressViewGwtImpl addressView;

  /**
   * initialize date for tests.
   */
  @BeforeEach
  public void initMapping() {
    when(uiBinderWidget.asWidget()).thenReturn(uiBinderWidget);
    when(postOfficeBox.asEditor()).thenReturn(ValueBoxEditor.of(postOfficeBox));
    doAnswer(answer -> when(postOfficeBox.getValueOrThrow())
        .thenReturn((String) answer.getArguments()[0])).when(postOfficeBox).setValue(any());
    when(street.asEditor()).thenReturn(ValueBoxEditor.of(street));
    doAnswer(answer -> when(street.getValueOrThrow()).thenReturn((String) answer.getArguments()[0]))
        .when(street).setValue(any());
    when(streetNumber.asEditor()).thenReturn(ValueBoxEditor.of(streetNumber));
    doAnswer(answer -> when(streetNumber.getValueOrThrow())
        .thenReturn((String) answer.getArguments()[0])).when(streetNumber).setValue(any());
    when(streetNumberAdditional.asEditor()).thenReturn(ValueBoxEditor.of(streetNumberAdditional));
    doAnswer(answer -> when(streetNumberAdditional.getValueOrThrow())
        .thenReturn((String) answer.getArguments()[0])).when(streetNumberAdditional)
            .setValue(any());
    when(extended.asEditor()).thenReturn(ValueBoxEditor.of(extended));
    doAnswer(
        answer -> when(extended.getValueOrThrow()).thenReturn((String) answer.getArguments()[0]))
            .when(extended).setValue(any());
    when(postalCode.asEditor()).thenReturn(ValueBoxEditor.of(postalCode));
    doAnswer(
        answer -> when(postalCode.getValueOrThrow()).thenReturn((String) answer.getArguments()[0]))
            .when(postalCode).setValue(any());
    when(locality.asEditor()).thenReturn(ValueBoxEditor.of(locality));
    doAnswer(
        answer -> when(locality.getValueOrThrow()).thenReturn((String) answer.getArguments()[0]))
            .when(locality).setValue(any());
    when(region.asEditor()).thenReturn(ValueBoxEditor.of(region));
    doAnswer(answer -> when(region.getValueOrThrow()).thenReturn((String) answer.getArguments()[0]))
        .when(region).setValue(any());
    when(countryCode.asEditor()).thenReturn(new ExtendedValueBoxEditor<>(countryCode, null));
    doAnswer(
        answer -> when(countryCode.getValue()).thenReturn((CountryEnum) answer.getArguments()[0]))
            .when(countryCode).setValue(any());
    when(uiBinder.createAndBindUi(any())).thenAnswer(answer -> {
      final AddressViewGwtImpl addressView = answer.getArgument(0);
      addressView.postOfficeBox = postOfficeBox;
      addressView.street = street;
      addressView.streetNumber = streetNumber;
      addressView.streetNumberAdditional = streetNumberAdditional;
      addressView.extended = extended;
      addressView.postalCode = postalCode;
      addressView.locality = locality;
      addressView.region = region;
      addressView.countryCode = countryCode;
      addressView.logMessages = logMessages;
      addressView.addressButton = addressButton;

      return uiBinderWidget;
    });

    driver = new AddressViewGwtImpl_Driver_Impl();
    addressView = new AddressViewGwtImpl(driver, uiBinder);
  }

  @Test
  public void onFormSubmitTest() {
    // given
    addressView.setPresenter(addressPresenter);

    // when
    addressView.onFormSubmit(null);

    // then
    verify(addressPresenter).tryToSend();
  }

  @Test
  public void showMessageTest() {
    // given
    final String message = "test";

    // when
    addressView.showMessage(message);

    // then
    verify(addressView.logMessages).setText(eq(message));
  }

  @Test
  public void setAndValidateEmptyFormTest() {
    // given
    final PostalAddressData addressData = new PostalAddressData();

    // when
    addressView.fillForm(addressData);

    // then
    verify(addressView.postOfficeBox).setValue(eq(addressData.getPostOfficeBox()));
    verify(addressView.street).setValue(eq(addressData.getStreet()));
    verify(addressView.streetNumber).setValue(eq(addressData.getStreetNumber()));
    verify(addressView.streetNumberAdditional)
        .setValue(eq(addressData.getStreetNumberAdditional()));
    verify(addressView.extended).setValue(eq(addressData.getExtended()));
    verify(addressView.postalCode).setValue(eq(addressData.getPostalCode()));
    verify(addressView.locality).setValue(eq(addressData.getLocality()));
    verify(addressView.region).setValue(eq(addressData.getRegion()));
    verify(addressView.countryCode).setValue(eq(addressData.getCountryCode()));

    verify(addressView.postOfficeBox, times(1))
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.street, times(1))
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.streetNumber, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.streetNumberAdditional, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.extended, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.postalCode, times(1))
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.locality, times(1))
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.region, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.countryCode, times(1))
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
  }

  @Test
  public void setAndValidateValidCountryFormTest() {
    // given
    final PostalAddressData addressData = new PostalAddressData();
    addressData.setPostalCode("12345");
    addressData.setLocality("Testcity");
    addressData.setStreet("Teststreet");
    addressData.setStreetNumber("1");
    addressData.setCountryCode(CountryEnum.DE);

    // when
    addressView.fillForm(addressData);

    // then
    verify(addressView.postOfficeBox).setValue(eq(addressData.getPostOfficeBox()));
    verify(addressView.street).setValue(eq(addressData.getStreet()));
    verify(addressView.streetNumber).setValue(eq(addressData.getStreetNumber()));
    verify(addressView.streetNumberAdditional)
        .setValue(eq(addressData.getStreetNumberAdditional()));
    verify(addressView.extended).setValue(eq(addressData.getExtended()));
    verify(addressView.postalCode).setValue(eq(addressData.getPostalCode()));
    verify(addressView.locality).setValue(eq(addressData.getLocality()));
    verify(addressView.region).setValue(eq(addressData.getRegion()));
    verify(addressView.countryCode).setValue(eq(addressData.getCountryCode()));

    verify(addressView.postOfficeBox, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.street, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.streetNumber, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.streetNumberAdditional, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.extended, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.postalCode, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.locality, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.region, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
    verify(addressView.countryCode, never())
        .showErrors(ArgumentMatchers.<List<EditorError>>argThat(list -> list.size() > 0));
  }
}
