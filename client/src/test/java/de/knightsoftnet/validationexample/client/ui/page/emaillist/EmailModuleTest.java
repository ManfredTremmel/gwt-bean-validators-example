package de.knightsoftnet.validationexample.client.ui.page.emaillist;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.knightsoftnet.validationexample.client.ui.page.emaillist.EmailPresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.page.emaillist.EmailPresenter.MyView;
import de.knightsoftnet.validationexample.client.ui.page.emaillist.EmailViewGwtImpl.Driver;

import com.google.gwt.inject.client.binder.GinAnnotatedBindingBuilder;
import com.google.gwt.inject.client.binder.GinBinder;
import com.google.gwt.inject.client.binder.GinScopedBindingBuilder;
import com.google.inject.Singleton;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * tests for EmailModule.
 *
 * @author Manfred Tremmel
 */
public class EmailModuleTest {

  @Mock
  private GinBinder binder;
  @SuppressWarnings("rawtypes")
  @Mock
  private GinAnnotatedBindingBuilder annotationBuilder;
  @Mock
  private GinScopedBindingBuilder scopeBinder;

  /**
   * initialize date for tests.
   */
  @SuppressWarnings("unchecked")
  @BeforeEach
  public void initMapping() {
    MockitoAnnotations.openMocks(this);
    when(binder.bind(any(Class.class))).thenReturn(annotationBuilder);
    when(annotationBuilder.to(any(Class.class))).thenReturn(scopeBinder);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void bindTest() {
    // given
    final EmailModule module = new EmailModule();

    // when
    module.configure(binder);

    // then
    verify(binder).bind(EmailPresenter.class);
    verify(binder).bind(MyView.class);
    verify(binder).bind(EmailViewGwtImpl.class);
    verify(annotationBuilder).to(EmailViewGwtImpl.class);
    verify(annotationBuilder, times(2)).in(Singleton.class);
    verify(binder).bind(MyProxy.class);
    verify(annotationBuilder).asEagerSingleton();
    verify(binder).bind(Driver.class);
    verify(annotationBuilder).to(EmailViewGwtImpl_Driver_Impl.class);
    verify(binder).bind(EmailListEditor.Driver.class);
    verify(annotationBuilder).to(EmailListEditor_Driver_Impl.class);
  }
}
