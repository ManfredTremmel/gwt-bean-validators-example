/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.address;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyIterable;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.validationexample.client.services.PostalAddressRestService;
import de.knightsoftnet.validationexample.client.ui.page.address.AddressPresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.page.address.AddressPresenter.MyView;
import de.knightsoftnet.validationexample.gwtmockito.GwtMockitoExtension;
import de.knightsoftnet.validationexample.shared.models.PostalAddressData;

import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;
import com.gwtplatform.dispatch.rest.delegates.test.DelegateTestUtils;
import com.gwtplatform.dispatch.shared.ActionException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

/**
 * tests for Address Presenter.
 *
 * @author Manfred Tremmel
 */
@ExtendWith(GwtMockitoExtension.class)
public class AddressPresenterTest {
  private static final String VALIDATION_ERROR_JSON = """
      {
          "validationErrorSet": [
              {
                  "message": "darf nicht leer sein",
                  "propertyPath": "postalCode"
              }
          ]
      }""";

  @Mock
  EventBus eventBus;
  @Mock
  MyView view;
  @Mock
  MyProxy proxy;
  @Mock
  AddressConstants constants;
  @Mock
  ResourceDelegate<PostalAddressRestService> postalAddressRestDelegate;
  @Mock
  PostalAddressRestService postalAddressRestService;
  @Mock
  Session session;
  @Mock
  PostalAddressData addressData;

  AddressPresenter addressPresenter;

  @Mock
  Response response;
  @Mock
  ActionException actionException;

  /**
   * initialize date for tests.
   */
  @BeforeEach
  public void initMapping() {
    when(postalAddressRestDelegate.withCallback(any(AsyncCallback.class)))
        .thenReturn(postalAddressRestService);
    when(constants.defaultCountry()).thenReturn("DE");
    when(constants.messageAddressDataOk()).thenReturn("OK");
    addressPresenter = new AddressPresenter(eventBus, view, proxy, constants,
        postalAddressRestDelegate, session, addressData);
  }

  @Test
  public void tryToSendSuccessTest() {
    // given
    DelegateTestUtils.givenDelegate(postalAddressRestDelegate)
        .useResource(PostalAddressRestService.class).and().succeed().when()
        .checkPostalAddress(addressData);

    // when
    addressPresenter.tryToSend();

    // then
    verify(view).showMessage(eq(constants.messageAddressDataOk()));
  }

  @Test
  public void tryToSendValidationErrorServersideTest() {
    // given
    when(response.getStatusCode()).thenReturn(400);
    when(response.getText()).thenReturn(VALIDATION_ERROR_JSON);
    DelegateTestUtils.givenDelegate(postalAddressRestDelegate)
        .useResource(PostalAddressRestService.class).and().fail().withResponse(response).and()
        .withThrowable(actionException).when().checkPostalAddress(addressData);

    // when
    addressPresenter.tryToSend();

    // then
    verify(view).setConstraintViolations(anyIterable());
  }

  @Test
  public void tryToSendErrorServersideTest() {
    // given
    when(response.getStatusCode()).thenReturn(500);
    DelegateTestUtils.givenDelegate(postalAddressRestDelegate)
        .useResource(PostalAddressRestService.class).and().fail().withResponse(response).and()
        .withThrowable(actionException).when().checkPostalAddress(addressData);

    // when
    addressPresenter.tryToSend();

    // then
    verify(view).showMessage(eq("messageHttpCode(500)"));
  }
}
