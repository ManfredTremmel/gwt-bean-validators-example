/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.login;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.knightsoftnet.gwtp.spring.client.rest.helper.HttpMessages;
import de.knightsoftnet.gwtp.spring.client.rest.helper.LoginMessages;
import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.validationexample.client.services.UserRestService;
import de.knightsoftnet.validationexample.client.ui.page.login.LoginPresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.page.login.LoginPresenter.MyView;
import de.knightsoftnet.validationexample.gwtmockito.GwtMockitoExtension;
import de.knightsoftnet.validationexample.shared.models.LoginData;
import de.knightsoftnet.validationexample.shared.models.UserData;

import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rest.delegates.client.AbstractResourceDelegate;
import com.gwtplatform.dispatch.rest.delegates.test.DelegateTestUtils;
import com.gwtplatform.dispatch.shared.ActionException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

/**
 * tests for Login Presenter.
 *
 * @author Manfred Tremmel
 */
@ExtendWith(GwtMockitoExtension.class)
public class LoginPresenterTest {
  private static final String USER = "user";
  private static final String PASSWORD = "pwd";

  @Mock
  EventBus eventBus;
  @Mock
  MyView view;
  @Mock
  MyProxy proxy;
  @Mock
  AbstractResourceDelegate<UserRestService> userService;
  @Mock
  UserRestService userRestService;
  @Mock
  Session session;
  @Mock
  LoginData loginData;

  LoginPresenter loginPresenter;

  @Mock
  UserData userData;
  @Mock
  Response response;
  @Mock
  ActionException actionException;
  @Mock
  private LoginMessages messages;
  @Mock
  private HttpMessages httpMessages;

  /**
   * initialize date for tests.
   */
  @BeforeEach
  public void initMapping() {
    when(userService.withCallback(any(AsyncCallback.class))).thenReturn(userRestService);
    when(userRestService.login(eq(USER), eq(PASSWORD))).thenReturn(userData);
    when(userData.getUserName()).thenReturn(USER);
    when(userData.getPassword()).thenReturn(PASSWORD);
    loginPresenter = new LoginPresenter(eventBus, view, proxy, userService, session, loginData);
  }

  @Test
  public void onRevealTest() {
    // given

    // when
    loginPresenter.onReveal();

    // then
    verify(loginData).clear();
    verify(view, times(2)).fillForm(loginData);
  }

  @Test
  public void tryToLoginSuccessTest() {
    // given
    DelegateTestUtils.givenDelegate(userService).useResource(UserRestService.class).and().succeed()
        .withResult(userData).when().login(loginData.getUserName(), loginData.getPassword());

    // when
    loginPresenter.tryToLogin();

    // then
    verify(session).setUser(userData);
  }

  @Test
  public void tryToLoginFailsTest() {
    // given
    when(response.getStatusCode()).thenReturn(401);
    when(response.getText()).thenReturn("401");
    DelegateTestUtils.givenDelegate(userService).useResource(UserRestService.class).and().fail()
        .withResponse(response).and().withThrowable(actionException).when()
        .login(loginData.getUserName(), loginData.getPassword());

    // when
    loginPresenter.tryToLogin();

    // then
    verify(view).showMessage(eq("messageLoginError(401)"));
    verify(session, never()).setUser(any(UserData.class));
  }

  @Test
  public void tryToLoginForbiddenTest() {
    // given
    when(response.getStatusCode()).thenReturn(403);
    DelegateTestUtils.givenDelegate(userService).useResource(UserRestService.class).and().fail()
        .withResponse(response).and().withThrowable(actionException).when()
        .login(loginData.getUserName(), loginData.getPassword());

    // when
    loginPresenter.tryToLogin();

    // then
    verify(session).readSessionData();
  }
}
