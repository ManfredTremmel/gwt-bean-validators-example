/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.phonenumber;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyIterable;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.validationexample.client.services.PhoneRestService;
import de.knightsoftnet.validationexample.client.ui.page.phonenumber.PhoneNumberPresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.page.phonenumber.PhoneNumberPresenter.MyView;
import de.knightsoftnet.validationexample.gwtmockito.GwtMockitoExtension;
import de.knightsoftnet.validationexample.shared.models.PhoneNumberData;

import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;
import com.gwtplatform.dispatch.rest.delegates.test.DelegateTestUtils;
import com.gwtplatform.dispatch.shared.ActionException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

/**
 * tests for PhoneNumber Presenter.
 *
 * @author Manfred Tremmel
 */
@ExtendWith(GwtMockitoExtension.class)
public class PhoneNumberPresenterTest {
  private static final String VALIDATION_ERROR_JSON = """
      {
          "validationErrorSet": [
              {
                  "message": "darf nicht leer sein",
                  "propertyPath": "phoneNumber"
              }
          ]
      }""";

  @Mock
  EventBus eventBus;
  @Mock
  MyView view;
  @Mock
  MyProxy proxy;
  @Mock
  PhoneNumberConstants constants;
  @Mock
  ResourceDelegate<PhoneRestService> phoneNumberRestDelegate;
  @Mock
  PhoneRestService phoneRestService;
  @Mock
  Session session;
  @Mock
  PhoneNumberData phoneNumberData;

  PhoneNumberPresenter phoneNumberPresenter;

  @Mock
  Response response;
  @Mock
  ActionException actionException;

  /**
   * initialize date for tests.
   */
  @BeforeEach
  public void initMapping() {
    when(phoneNumberRestDelegate.withCallback(any(AsyncCallback.class)))
        .thenReturn(phoneRestService);
    when(constants.defaultCountry()).thenReturn("DE");
    when(constants.messagePhoneNumberOk()).thenReturn("OK");
    phoneNumberPresenter = new PhoneNumberPresenter(eventBus, view, proxy, constants,
        phoneNumberRestDelegate, session, phoneNumberData);
  }

  @Test
  public void tryToSendSuccessTest() {
    // given
    DelegateTestUtils.givenDelegate(phoneNumberRestDelegate).useResource(PhoneRestService.class)
        .and().succeed().when().checkPhoneNumber(phoneNumberData);

    // when
    phoneNumberPresenter.tryToSend();

    // then
    verify(view).showMessage(eq(constants.messagePhoneNumberOk()));
  }

  @Test
  public void tryToSendValidationErrorServersideTest() {
    // given
    when(response.getStatusCode()).thenReturn(400);
    when(response.getText()).thenReturn(VALIDATION_ERROR_JSON);
    DelegateTestUtils.givenDelegate(phoneNumberRestDelegate).useResource(PhoneRestService.class)
        .and().fail().withResponse(response).and().withThrowable(actionException).when()
        .checkPhoneNumber(phoneNumberData);

    // when
    phoneNumberPresenter.tryToSend();

    // then
    verify(view).setConstraintViolations(anyIterable());
  }

  @Test
  public void tryToSendErrorServersideTest() {
    // given
    when(response.getStatusCode()).thenReturn(500);
    DelegateTestUtils.givenDelegate(phoneNumberRestDelegate).useResource(PhoneRestService.class)
        .and().fail().withResponse(response).and().withThrowable(actionException).when()
        .checkPhoneNumber(phoneNumberData);

    // when
    phoneNumberPresenter.tryToSend();

    // then
    verify(view).showMessage(eq("messageHttpCode(500)"));
  }
}
