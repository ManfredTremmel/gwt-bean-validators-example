package de.knightsoftnet.validationexample.client.ui.navigation;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.knightsoftnet.navigation.client.ui.navigation.NavigationPresenter;
import de.knightsoftnet.navigation.client.ui.navigation.NavigationPresenter.MyProxy;
import de.knightsoftnet.navigation.client.ui.navigation.NavigationPresenter.MyView;
import de.knightsoftnet.navigation.client.ui.navigation.TreeNavigationView;

import com.google.gwt.inject.client.binder.GinAnnotatedBindingBuilder;
import com.google.gwt.inject.client.binder.GinBinder;
import com.google.gwt.inject.client.binder.GinScopedBindingBuilder;
import com.google.inject.Singleton;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * tests for NavigationModule.
 *
 * @author Manfred Tremmel
 */
public class NavigationModuleTest {

  @Mock
  private GinBinder binder;
  @SuppressWarnings("rawtypes")
  @Mock
  private GinAnnotatedBindingBuilder annotationBuilder;
  @Mock
  private GinScopedBindingBuilder scopeBinder;

  /**
   * initialize date for tests.
   */
  @SuppressWarnings("unchecked")
  @BeforeEach
  public void initMapping() {
    MockitoAnnotations.openMocks(this);
    when(binder.bind(any(Class.class))).thenReturn(annotationBuilder);
    when(annotationBuilder.to(any(Class.class))).thenReturn(scopeBinder);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void bindTest() {
    // given
    final NavigationModule module = new NavigationModule();

    // when
    module.configure(binder);

    // then
    verify(binder).bind(NavigationPresenter.class);
    verify(binder).bind(MyView.class);
    verify(binder).bind(TreeNavigationView.class);
    verify(annotationBuilder).to(TreeNavigationView.class);
    verify(annotationBuilder, times(2)).in(Singleton.class);
    verify(binder).bind(MyProxy.class);
    verify(annotationBuilder).asEagerSingleton();
  }
}
