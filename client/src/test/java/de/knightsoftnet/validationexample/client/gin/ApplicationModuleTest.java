/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.gin;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.gwtp.spring.shared.models.User;
import de.knightsoftnet.mtwidgets.client.ui.request.DeleteRequestModule;
import de.knightsoftnet.navigation.client.ui.navigation.NavigationStructure;
import de.knightsoftnet.validationexample.client.ui.basepage.BasePageModule;
import de.knightsoftnet.validationexample.client.ui.basepage.about.AboutPopUpModule;
import de.knightsoftnet.validationexample.client.ui.navigation.NavigationModule;
import de.knightsoftnet.validationexample.client.ui.page.address.AddressModule;
import de.knightsoftnet.validationexample.client.ui.page.emaillist.EmailModule;
import de.knightsoftnet.validationexample.client.ui.page.login.LoginModule;
import de.knightsoftnet.validationexample.client.ui.page.logout.LogoutModule;
import de.knightsoftnet.validationexample.client.ui.page.person.CostCenterEntityModule;
import de.knightsoftnet.validationexample.client.ui.page.person.PersonModule;
import de.knightsoftnet.validationexample.client.ui.page.phonenumber.PhoneNumberModule;
import de.knightsoftnet.validationexample.client.ui.page.secret.SecretModule;
import de.knightsoftnet.validationexample.client.ui.page.sepa.SepaModule;
import de.knightsoftnet.validationexample.client.ui.page.settings.SettingsModule;

import com.google.gwt.inject.client.binder.GinAnnotatedBindingBuilder;
import com.google.gwt.inject.client.binder.GinBinder;
import com.google.gwt.inject.client.binder.GinScopedBindingBuilder;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * tests for ApplicationModule.
 *
 * @author Manfred Tremmel
 */
public class ApplicationModuleTest {

  @Mock
  private GinBinder binder;
  @SuppressWarnings("rawtypes")
  @Mock
  private GinAnnotatedBindingBuilder annotationBuilder;
  @Mock
  private GinScopedBindingBuilder scopedBuilder;

  /**
   * initialize date for tests.
   */
  @SuppressWarnings("unchecked")
  @BeforeEach
  public void initMapping() {
    MockitoAnnotations.openMocks(this);
    when(binder.bind(ArgumentMatchers.any(Class.class))).thenReturn(annotationBuilder);
    when(annotationBuilder.to(ArgumentMatchers.any(Class.class))).thenReturn(scopedBuilder);
  }

  @Test
  public void bindTest() {
    // given
    final ApplicationModule module = new ApplicationModule();

    // when
    module.configure(binder);

    // then
    verify(binder, Mockito.times(14)).install(ArgumentMatchers.any());
    verify(binder).install(any(BasePageModule.class));
    verify(binder).install(any(NavigationModule.class));
    verify(binder).install(any(DeleteRequestModule.class));
    verify(binder).install(any(AboutPopUpModule.class));
    verify(binder).install(any(AddressModule.class));
    verify(binder).install(any(CostCenterEntityModule.class));
    verify(binder).install(any(EmailModule.class));
    verify(binder).install(any(LoginModule.class));
    verify(binder).install(any(LogoutModule.class));
    verify(binder).install(any(PersonModule.class));
    verify(binder).install(any(PhoneNumberModule.class));
    verify(binder).install(any(SecretModule.class));
    verify(binder).install(any(SepaModule.class));
    verify(binder).install(any(SettingsModule.class));
    verify(binder).bind(User.class);
    // .to(UserData.class).in(Singleton.class);
    verify(binder).bind(Session.class);
    // .to(CurrentSession.class).in(Singleton.class);
    verify(binder).bind(NavigationStructure.class);
    // .to(MyNavigationStructure.class).in(Singleton.class);
  }
}
