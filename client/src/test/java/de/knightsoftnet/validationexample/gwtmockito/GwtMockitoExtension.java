package de.knightsoftnet.validationexample.gwtmockito;

import com.google.gwtmockito.GwtMockito;

import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class GwtMockitoExtension implements BeforeEachCallback, AfterEachCallback {

  @Override
  public void beforeEach(final ExtensionContext context) {
    // Invoke initMocks on the version of GwtMockito that was loaded via our custom classloader.
    // This is necessary to ensure that it uses the same set of classes as the unit test class,
    // which we loaded through the custom classloader above.
    GwtMockito.initMocks(context.getTestInstance().orElseThrow());
  }

  @Override
  public void afterEach(final ExtensionContext context) {
    GwtMockito.tearDown();
  }
}
