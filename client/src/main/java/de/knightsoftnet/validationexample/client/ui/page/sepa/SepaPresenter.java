/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.sepa;

import de.knightsoftnet.gwtp.spring.client.rest.helper.AbstractPresenterWithErrorHandling;
import de.knightsoftnet.gwtp.spring.client.rest.helper.EditorWithErrorHandling;
import de.knightsoftnet.gwtp.spring.client.rest.helper.RestCallbackBuilder;
import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.navigation.client.ui.basepage.AbstractBasePagePresenter;
import de.knightsoftnet.validationexample.client.services.SepaRestService;
import de.knightsoftnet.validationexample.client.ui.page.sepa.SepaPresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.page.sepa.SepaPresenter.MyView;
import de.knightsoftnet.validationexample.shared.models.SepaData;
import de.knightsoftnet.validationexample.shared.navigation.NameTokens;
import de.knightsoftnet.validators.client.event.FormSubmitHandler;
import de.knightsoftnet.validators.shared.data.CountryEnum;
import de.knightsoftnet.validators.shared.data.IbanLengthMapSharedConstants;
import de.knightsoftnet.validators.shared.util.HasSetIbanLengthMapSharedConstants;
import de.knightsoftnet.validators.shared.util.IbanUtil;

import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

import java.util.Collection;

import jakarta.inject.Inject;

/**
 * Presenter of the Sepa, implementation.
 *
 * @author Manfred Tremmel
 *
 */
public class SepaPresenter extends AbstractPresenterWithErrorHandling<MyProxy, MyView, SepaData>
    implements HasSetIbanLengthMapSharedConstants {

  public interface MyView
      extends EditorWithErrorHandling<SepaPresenter, SepaData>, FormSubmitHandler<SepaData> {
    void setValidCountries(Collection<CountryEnum> countries);
  }

  @ProxyCodeSplit
  @NameToken(NameTokens.SEPA)
  @NoGatekeeper
  public interface MyProxy extends ProxyPlace<SepaPresenter> {
  }

  private final SepaData sepaData;

  private final SepaConstants constants;
  private final ResourceDelegate<SepaRestService> sepaRestDelegate;
  private final Session session;
  private final IbanUtil ibanUtil;

  /**
   * constructor injecting parameters.
   */
  @Inject
  public SepaPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
      final SepaConstants constants, final ResourceDelegate<SepaRestService> sepaRestDelegate,
      final Session session, final SepaData sepaData) {
    super(eventBus, view, proxy, AbstractBasePagePresenter.SLOT_MAIN_CONTENT);
    this.constants = constants;
    this.sepaRestDelegate = sepaRestDelegate;
    this.session = session;
    ibanUtil = new IbanUtil();
    ibanUtil.setIbanLengthMapSharedConstantsWhenAvailable(this);
    this.sepaData = sepaData;
    this.sepaData.setCountryCode(CountryEnum.valueOf(constants.defaultCountry()));
    getView().setPresenter(this);
    getView().fillForm(this.sepaData);
  }

  /**
   * try to send data.
   */
  public void tryToSend() {
    sepaRestDelegate.withCallback(RestCallbackBuilder.build(getView(), sepaData, session,
        result -> getView().showMessage(constants.messageSepaOk()))).checkSepa(sepaData);
  }

  @Override
  public void setIbanLengthMapSharedConstants(
      IbanLengthMapSharedConstants ibanLengthMapSharedConstants) {
    getView().setValidCountries(ibanLengthMapSharedConstants.getIbanLengthMap().keySet());
  }
}
