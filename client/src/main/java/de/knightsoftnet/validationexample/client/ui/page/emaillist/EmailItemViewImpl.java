/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.emaillist;

import de.knightsoftnet.mtwidgets.client.ui.widget.EmailTextBox;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.AbstractListItemView;
import de.knightsoftnet.validationexample.client.ui.widget.EmailTypeListBox;
import de.knightsoftnet.validationexample.shared.models.EmailData;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Widget;

import jakarta.inject.Inject;
import jakarta.inject.Provider;

/**
 * View of the email item, gwt implementation.
 *
 * @author Manfred Tremmel
 */
public class EmailItemViewImpl extends AbstractListItemView<EmailData> {

  interface Binder extends UiBinder<Widget, EmailItemViewImpl> {
  }

  @UiField
  EmailTextBox email;

  @UiField
  EmailTypeListBox type;

  private final Provider<EmailTypeListBox> emailTypeListBoxProvider;

  /**
   * constructor.
   */
  @Inject
  public EmailItemViewImpl(final Binder uiBinder,
      final Provider<EmailTypeListBox> emailTypeListBoxProvider) {
    super();
    this.emailTypeListBoxProvider = emailTypeListBoxProvider;
    initWidget(uiBinder.createAndBindUi(this));
  }

  @Ignore
  @UiFactory
  public EmailTypeListBox buildEmailTypeListBox() {
    return emailTypeListBoxProvider.get();
  }

  @UiHandler("deleteRow")
  public void onDeleteRow(final ClickEvent event) {
    removeThisEntry();
  }
}
