/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.emaillist;

import de.knightsoftnet.mtwidgets.client.ui.widget.helper.AbstractListEditorWithProvider;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.ItemEditorSourceWithProvider;
import de.knightsoftnet.validationexample.shared.models.EmailData;
import de.knightsoftnet.validators.client.editor.impl.ListValidationEditor;

import org.gwtproject.editor.client.SimpleBeanEditorDriver;
import org.gwtproject.editor.client.annotation.IsDriver;

import java.util.List;

import jakarta.inject.Inject;
import jakarta.inject.Provider;

/**
 * editor to edit a list of email entries.
 *
 * @author Manfred Tremmel
 */
public class EmailListEditor extends AbstractListEditorWithProvider<EmailData, EmailItemViewImpl> {

  /**
   * interface of the driver to combine ui and bean.
   */
  @IsDriver
  interface Driver extends SimpleBeanEditorDriver<List<EmailData>, //
      ListValidationEditor<EmailData, EmailItemViewImpl>> {
  }

  private final ListValidationEditor<EmailData, EmailItemViewImpl> editor;

  /**
   * constructor.
   *
   */
  @Inject
  public EmailListEditor(final Driver driver, final Provider<EmailData> dataProvider,
      final Provider<EmailItemViewImpl> viewProvider) {
    super(dataProvider);
    editor = ListValidationEditor.of(new ItemEditorSourceWithProvider<>(this, viewProvider));
    driver.initialize(editor);
  }

  @Override
  public ListValidationEditor<EmailData, EmailItemViewImpl> asEditor() {
    return editor;
  }
}
