/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.basepage;

import de.knightsoftnet.navigation.client.ui.basepage.AbstractBasePagePresenter;
import de.knightsoftnet.navigation.client.ui.navigation.NavigationPresenter;
import de.knightsoftnet.validationexample.client.ui.basepage.BasePagePresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.basepage.BasePagePresenter.MyView;
import de.knightsoftnet.validationexample.client.ui.basepage.about.AboutPresenter;

import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.presenter.slots.PermanentSlot;
import com.gwtplatform.mvp.client.proxy.Proxy;

import jakarta.inject.Inject;

/**
 * Presenter of the base page, which embeds the other components.
 *
 * @author Manfred Tremmel
 *
 */
public class BasePagePresenter extends AbstractBasePagePresenter<MyView, MyProxy> {

  public interface MyView extends View {
    void setPresenter(BasePagePresenter ppresenter);
  }

  @ProxyStandard
  @NoGatekeeper
  public interface MyProxy extends Proxy<BasePagePresenter> {
  }

  public static final PermanentSlot<NavigationPresenter> SLOT_NAVIGATION = new PermanentSlot<>();

  protected final NavigationPresenter navigationPresenter;
  protected final AboutPresenter aboutPresenter;

  /**
   * constructor getting parameters injected.
   *
   * @param eventBus event bus
   * @param view view of this page
   * @param aboutPresenter presenter of the about popup
   * @param proxy proxy to handle page
   * @param navigationPresenter navigation presenter
   */
  @Inject
  public BasePagePresenter(final EventBus eventBus, final MyView view,
      final AboutPresenter aboutPresenter, final MyProxy proxy,
      final NavigationPresenter navigationPresenter) {
    super(eventBus, view, proxy);
    this.aboutPresenter = aboutPresenter;
    this.navigationPresenter = navigationPresenter;
    view.setPresenter(this);
  }

  @Override
  protected void onBind() {
    setInSlot(SLOT_NAVIGATION, navigationPresenter);
  }

  /**
   * show info dialog.
   */
  public void showInfo() {
    addToPopupSlot(aboutPresenter);
  }
}
