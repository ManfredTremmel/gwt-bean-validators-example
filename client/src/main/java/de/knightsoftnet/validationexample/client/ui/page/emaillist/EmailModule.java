/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.emaillist;

import de.knightsoftnet.validationexample.client.ui.page.emaillist.EmailPresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.page.emaillist.EmailPresenter.MyView;
import de.knightsoftnet.validationexample.client.ui.page.emaillist.EmailViewGwtImpl.Driver;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

import jakarta.inject.Singleton;

public class EmailModule extends AbstractPresenterModule {

  @Override
  protected void configure() {
    bindPresenter(EmailPresenter.class, MyView.class, EmailViewGwtImpl.class, MyProxy.class);
    bind(Driver.class).to(EmailViewGwtImpl_Driver_Impl.class).in(Singleton.class);
    bind(EmailListEditor.Driver.class).to(EmailListEditor_Driver_Impl.class).in(Singleton.class);
  }
}
