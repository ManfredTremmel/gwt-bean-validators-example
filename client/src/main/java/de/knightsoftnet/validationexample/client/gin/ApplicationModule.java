/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.gin;

import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.gwtp.spring.shared.models.User;
import de.knightsoftnet.mtwidgets.client.ui.request.DeleteRequestModule;
import de.knightsoftnet.navigation.client.ui.navigation.NavigationStructure;
import de.knightsoftnet.validationexample.client.CurrentSession;
import de.knightsoftnet.validationexample.client.ui.basepage.BasePageModule;
import de.knightsoftnet.validationexample.client.ui.basepage.about.AboutPopUpModule;
import de.knightsoftnet.validationexample.client.ui.navigation.MyNavigationStructure;
import de.knightsoftnet.validationexample.client.ui.navigation.NavigationModule;
import de.knightsoftnet.validationexample.client.ui.page.address.AddressModule;
import de.knightsoftnet.validationexample.client.ui.page.emaillist.EmailModule;
import de.knightsoftnet.validationexample.client.ui.page.login.LoginModule;
import de.knightsoftnet.validationexample.client.ui.page.logout.LogoutModule;
import de.knightsoftnet.validationexample.client.ui.page.person.CostCenterEntityModule;
import de.knightsoftnet.validationexample.client.ui.page.person.PersonModule;
import de.knightsoftnet.validationexample.client.ui.page.phonenumber.PhoneNumberModule;
import de.knightsoftnet.validationexample.client.ui.page.secret.SecretModule;
import de.knightsoftnet.validationexample.client.ui.page.sepa.SepaModule;
import de.knightsoftnet.validationexample.client.ui.page.settings.SettingsModule;
import de.knightsoftnet.validationexample.shared.models.UserData;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

import jakarta.inject.Singleton;

public class ApplicationModule extends AbstractPresenterModule {

  @Override
  protected void configure() {

    install(new BasePageModule());
    install(new NavigationModule());
    install(new DeleteRequestModule());
    install(new AboutPopUpModule());
    install(new AddressModule());
    install(new CostCenterEntityModule());
    install(new EmailModule());
    install(new LoginModule());
    install(new LogoutModule());
    install(new PersonModule());
    install(new PhoneNumberModule());
    install(new SecretModule());
    install(new SepaModule());
    install(new SettingsModule());

    bind(User.class).to(UserData.class).in(Singleton.class);
    bind(Session.class).to(CurrentSession.class).in(Singleton.class);
    bind(NavigationStructure.class).to(MyNavigationStructure.class).in(Singleton.class);
  }
}
