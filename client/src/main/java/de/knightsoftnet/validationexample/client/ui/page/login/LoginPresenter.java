/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.login;

import de.knightsoftnet.gwtp.spring.client.rest.helper.AbstractPresenterWithErrorHandling;
import de.knightsoftnet.gwtp.spring.client.rest.helper.EditorWithErrorHandling;
import de.knightsoftnet.gwtp.spring.client.rest.helper.RestCallbackBuilder;
import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.navigation.client.ui.basepage.AbstractBasePagePresenter;
import de.knightsoftnet.validationexample.client.services.UserRestService;
import de.knightsoftnet.validationexample.client.ui.page.login.LoginPresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.page.login.LoginPresenter.MyView;
import de.knightsoftnet.validationexample.shared.models.LoginData;
import de.knightsoftnet.validationexample.shared.navigation.NameTokens;
import de.knightsoftnet.validators.client.event.FormSubmitHandler;

import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

import jakarta.inject.Inject;

/**
 * Presenter of the login, implementation.
 *
 * @author Manfred Tremmel
 *
 */
public class LoginPresenter extends AbstractPresenterWithErrorHandling<MyProxy, MyView, LoginData> {

  public interface MyView
      extends EditorWithErrorHandling<LoginPresenter, LoginData>, FormSubmitHandler<LoginData> {
  }

  @ProxyCodeSplit
  @NameToken(NameTokens.LOGIN)
  @NoGatekeeper
  public interface MyProxy extends ProxyPlace<LoginPresenter> {
  }

  private final Session session;

  private final ResourceDelegate<UserRestService> userService;

  /**
   * user data to remember.
   */
  private final LoginData loginData;

  /**
   * presenter with injected parameters.
   */
  @Inject
  public LoginPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
      final ResourceDelegate<UserRestService> userService, final Session session,
      final LoginData loginData) {
    super(eventBus, view, proxy, AbstractBasePagePresenter.SLOT_MAIN_CONTENT);
    this.userService = userService;
    this.session = session;
    this.loginData = loginData;
    getView().setPresenter(this);
    getView().fillForm(this.loginData);
  }

  @Override
  protected void onReveal() {
    super.onReveal();
    loginData.clear();
    getView().fillForm(loginData);
  }

  /**
   * try to login.
   */
  public void tryToLogin() {
    userService.withCallback(RestCallbackBuilder.buildLoginCallback(getView(), session))
        .login(loginData.getUserName(), loginData.getPassword());
  }
}
