/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.phonenumber;

import de.knightsoftnet.gwtp.spring.client.rest.helper.AbstractPresenterWithErrorHandling;
import de.knightsoftnet.gwtp.spring.client.rest.helper.EditorWithErrorHandling;
import de.knightsoftnet.gwtp.spring.client.rest.helper.RestCallbackBuilder;
import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.navigation.client.ui.basepage.AbstractBasePagePresenter;
import de.knightsoftnet.validationexample.client.services.PhoneRestService;
import de.knightsoftnet.validationexample.client.ui.page.phonenumber.PhoneNumberPresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.page.phonenumber.PhoneNumberPresenter.MyView;
import de.knightsoftnet.validationexample.shared.models.PhoneNumberData;
import de.knightsoftnet.validationexample.shared.navigation.NameTokens;
import de.knightsoftnet.validators.client.event.FormSubmitHandler;
import de.knightsoftnet.validators.shared.data.CountryEnum;

import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

import jakarta.inject.Inject;

/**
 * Presenter of the Sepa, implementation.
 *
 * @author Manfred Tremmel
 *
 */
public class PhoneNumberPresenter
    extends AbstractPresenterWithErrorHandling<MyProxy, MyView, PhoneNumberData> {

  public interface MyView extends EditorWithErrorHandling<PhoneNumberPresenter, PhoneNumberData>,
      FormSubmitHandler<PhoneNumberData> {
  }

  @ProxyCodeSplit
  @NameToken(NameTokens.PHONE_NUMBER)
  @NoGatekeeper
  public interface MyProxy extends ProxyPlace<PhoneNumberPresenter> {
  }

  private final PhoneNumberData phoneNumberData;

  private final PhoneNumberConstants constants;
  private final ResourceDelegate<PhoneRestService> phoneNumberRestDelegate;
  private final Session session;

  /**
   * constructor injecting parameters.
   */
  @Inject
  public PhoneNumberPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
      final PhoneNumberConstants constants,
      final ResourceDelegate<PhoneRestService> phoneNumberRestDelegate, final Session session,
      final PhoneNumberData phoneNumberData) {
    super(eventBus, view, proxy, AbstractBasePagePresenter.SLOT_MAIN_CONTENT);
    this.constants = constants;
    this.session = session;
    this.phoneNumberRestDelegate = phoneNumberRestDelegate;
    this.phoneNumberData = phoneNumberData;
    this.phoneNumberData.setCountryCode(CountryEnum.valueOf(constants.defaultCountry()));
    getView().setPresenter(this);
    getView().fillForm(this.phoneNumberData);
  }

  /**
   * try to send data.
   */
  public void tryToSend() {
    phoneNumberRestDelegate
        .withCallback(RestCallbackBuilder.build(getView(), phoneNumberData, session,
            result -> getView().showMessage(constants.messagePhoneNumberOk())))
        .checkPhoneNumber(phoneNumberData);
  }
}
