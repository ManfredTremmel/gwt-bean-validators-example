/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.person;

import static de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator.GSSS_UIB_COLUMN;
import static de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator.GSSS_UIB_COLUMN_12;
import static de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator.GSSS_UIB_RESOURCE_FILE;
import static de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator.GSSS_UIB_ROW;

import de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator;
import de.knightsoftnet.gwtp.spring.client.annotation.SearchField;
import de.knightsoftnet.validationexample.shared.AppResourcePaths;
import de.knightsoftnet.validationexample.shared.models.CostCenterEntity;
import de.knightsoftnet.validationexample.shared.models.Person;
import de.knightsoftnet.validationexample.shared.navigation.NameTokens;

/**
 * Dummy class to generate backoffice client stuff.
 */
public class GeneratingBackoffice {

  @BackofficeClientGenerator(//
      value = AppResourcePaths.COST_CENTER, //
      baseToken = NameTokens.COST_CENTER, //
      languages = {"en", "de"}, //
      searchResults = {//
          @SearchField(field = "id", gridColumns = 2),
          @SearchField(field = "number", gridColumns = 4),
          @SearchField(field = "name", gridColumns = 6)},
      uiBinderHeader = {GSSS_UIB_RESOURCE_FILE, "<ui:style src=\"../../basepage/Global.gss\"/>"},
      uiBinderStyleCentralPanel = "{style.centerPanel}",
      uiBinderStyleRow = GSSS_UIB_ROW + " {style.row}",
      uiBinderStyleHeader = GSSS_UIB_COLUMN + " " + GSSS_UIB_COLUMN_12 + " {style.header}",
      uiBinderErrorStyle = "{style.errorPanel}", //
      generateLocalizationProperties = false)
  private CostCenterEntity costCenter; // NOPMD

  @BackofficeClientGenerator(//
      value = AppResourcePaths.PERSON, //
      baseToken = NameTokens.PERSON, //
      languages = {"en", "de"}, //
      searchResults = {//
          @SearchField(field = "id", gridColumns = 1),
          @SearchField(field = "firstName", gridColumns = 3),
          @SearchField(field = "lastName", gridColumns = 3),
          @SearchField(field = "phoneNumber", gridColumns = 3),
          @SearchField(field = "birthday", gridColumns = 2)},
      uiBinderHeader = {GSSS_UIB_RESOURCE_FILE, "<ui:style src=\"../../basepage/Global.gss\"/>"},
      uiBinderStyleCentralPanel = "{style.centerPanel}",
      uiBinderStyleRow = GSSS_UIB_ROW + " {style.row}",
      uiBinderStyleHeader = GSSS_UIB_COLUMN + " " + GSSS_UIB_COLUMN_12 + " {style.header}",
      uiBinderErrorStyle = "{style.errorPanel}", //
      generateLocalizationProperties = false)
  private Person person; // NOPMD
}
