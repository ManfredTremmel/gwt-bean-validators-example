/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.phonenumber;

import de.knightsoftnet.validationexample.client.ui.page.phonenumber.PhoneNumberPresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.page.phonenumber.PhoneNumberPresenter.MyView;
import de.knightsoftnet.validationexample.client.ui.page.phonenumber.PhoneNumberViewGwtImpl.Driver;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

import jakarta.inject.Singleton;

public class PhoneNumberModule extends AbstractPresenterModule {

  @Override
  protected void configure() {
    bindPresenter(PhoneNumberPresenter.class, MyView.class, PhoneNumberViewGwtImpl.class,
        MyProxy.class);
    bind(Driver.class).to(PhoneNumberViewGwtImpl_Driver_Impl.class).in(Singleton.class);
  }
}
