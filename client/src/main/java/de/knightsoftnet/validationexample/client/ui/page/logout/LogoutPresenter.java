/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.logout;

import de.knightsoftnet.gwtp.spring.client.rest.helper.RestCallbackBuilder;
import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.navigation.client.ui.basepage.AbstractBasePagePresenter;
import de.knightsoftnet.validationexample.client.services.UserRestService;
import de.knightsoftnet.validationexample.client.ui.page.logout.LogoutPresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.page.logout.LogoutPresenter.MyView;
import de.knightsoftnet.validationexample.shared.navigation.NameTokens;

import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

import jakarta.inject.Inject;

/**
 * Presenter of the logout, implementation.
 *
 * @author Manfred Tremmel
 *
 */
public class LogoutPresenter extends Presenter<MyView, MyProxy> {

  public interface MyView extends View {
  }

  @ProxyCodeSplit
  @NameToken(NameTokens.LOGOUT)
  public interface MyProxy extends ProxyPlace<LogoutPresenter> {
  }

  private final ResourceDelegate<UserRestService> userService;

  private final Session session;

  /**
   * constructor injecting parameters.
   */
  @Inject
  public LogoutPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
      final ResourceDelegate<UserRestService> userService, final Session session) {
    super(eventBus, view, proxy, AbstractBasePagePresenter.SLOT_MAIN_CONTENT);
    this.userService = userService;
    this.session = session;
  }

  @Override
  protected void onReveal() {
    super.onReveal();
    userService.withCallback(RestCallbackBuilder.<Void>build(

        result -> session.readSessionData(), //
        failure -> session.readSessionData())).logout();
  }
}
