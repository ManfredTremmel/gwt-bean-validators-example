/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.shared.models;

import de.knightsoftnet.validators.shared.EmptyIfOtherIsNotEmpty;
import de.knightsoftnet.validators.shared.NotEmptyIfOtherIsEmpty;
import de.knightsoftnet.validators.shared.NotEmptyIfOtherIsNotEmpty;
import de.knightsoftnet.validators.shared.PostalCode;
import de.knightsoftnet.validators.shared.data.CountryEnum;

import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@EmptyIfOtherIsNotEmpty.List({
    // post office box and street can only be alternate filled, not both
    @EmptyIfOtherIsNotEmpty(field = "postOfficeBox", fieldCompare = "street"),
    @EmptyIfOtherIsNotEmpty(field = "street", fieldCompare = "postOfficeBox")})
@NotEmptyIfOtherIsEmpty.List({
    // post office box or street must be alternate be filled
    @NotEmptyIfOtherIsEmpty(field = "street", fieldCompare = "postOfficeBox"),
    @NotEmptyIfOtherIsEmpty(field = "postOfficeBox", fieldCompare = "street")})
@NotEmptyIfOtherIsNotEmpty.List({
    // street must be filled if street number is filled
    @NotEmptyIfOtherIsNotEmpty(field = "street", fieldCompare = "streetNumber"),
    // street number must be filled if street number additional is filled
    @NotEmptyIfOtherIsNotEmpty(field = "streetNumber", fieldCompare = "streetNumberAdditional")})
@PostalCode(fieldCountryCode = "countryCode", fieldPostalCode = "postalCode")
public class PostalAddressData {

  /**
   * default length limit.
   */
  public static final int LENGTH_DEFAULT = 255;

  /**
   * length limit of the street name.
   */
  public static final int LENGTH_STREET_NUMBER = 10;

  /**
   * length limit postal code.
   */
  public static final int LENGTH_POSTAL_CODE = 10;

  /**
   * post office box.
   */
  @Size(max = LENGTH_DEFAULT)
  private String postOfficeBox;

  /**
   * street name.
   */
  @Size(max = LENGTH_DEFAULT)
  private String street;

  /**
   * street number.
   */
  @Size(max = LENGTH_STREET_NUMBER)
  private String streetNumber;

  /**
   * street number additional.
   */
  @Size(max = LENGTH_STREET_NUMBER)
  private String streetNumberAdditional;

  /**
   * extended address data (some thing like apartment).
   */
  @Size(max = LENGTH_DEFAULT)
  private String extended;

  /**
   * postal code (zip).
   */
  @Size(max = LENGTH_POSTAL_CODE)
  @NotEmpty
  private String postalCode;

  /**
   * localized name of the locality (city/town).
   */
  @Size(max = LENGTH_DEFAULT)
  @NotEmpty
  private String locality;

  /**
   * localized name of the region (state).
   */
  @Size(max = LENGTH_DEFAULT)
  private String region;

  /**
   * country code.
   */
  @NotNull
  private CountryEnum countryCode;


  @Valid
  private final List<EmailData> emailList;


  public PostalAddressData() {
    super();
    emailList = new ArrayList<>();
  }

  public String getPostOfficeBox() {
    return postOfficeBox;
  }

  public void setPostOfficeBox(final String postOfficeBox) {
    this.postOfficeBox = postOfficeBox;
  }

  public String getExtended() {
    return extended;
  }

  public void setExtended(final String extended) {
    this.extended = extended;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(final String street) {
    this.street = street;
  }

  public String getStreetNumber() {
    return streetNumber;
  }

  public void setStreetNumber(final String streetNumber) {
    this.streetNumber = streetNumber;
  }

  public String getStreetNumberAdditional() {
    return streetNumberAdditional;
  }

  public void setStreetNumberAdditional(final String streetNumberAdditional) {
    this.streetNumberAdditional = streetNumberAdditional;
  }

  public String getLocality() {
    return locality;
  }

  public void setLocality(final String locality) {
    this.locality = locality;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(final String region) {
    this.region = region;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(final String postalCode) {
    this.postalCode = postalCode;
  }

  public CountryEnum getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(final CountryEnum countryCode) {
    this.countryCode = countryCode;
  }

  public final List<EmailData> getEmailList() {
    return emailList;
  }

  /**
   * set email list.
   *
   * @param emailList email list to set
   */
  public final void setEmailList(final List<EmailData> emailList) {
    this.emailList.clear();
    if (!CollectionUtils.isEmpty(emailList)) {
      this.emailList.addAll(emailList);
    }
  }

  @Override
  public String toString() {
    return "PostalAddressData [postOfficeBox=" + postOfficeBox + ", street=" + street
        + ", streetNumber=" + streetNumber + ", streetNumberAdditional=" + streetNumberAdditional
        + ", extended=" + extended + ", postalCode=" + postalCode + ", locality=" + locality
        + ", region=" + region + ", countryCode=" + countryCode + "]";
  }
}
