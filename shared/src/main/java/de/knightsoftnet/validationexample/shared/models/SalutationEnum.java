package de.knightsoftnet.validationexample.shared.models;

/**
 * possible salutations.
 *
 * @author Manfred Tremmel
 */
public enum SalutationEnum {
  // Mister
  MR,
  // Misses
  MRS,
  // Inter
  INTER
}
