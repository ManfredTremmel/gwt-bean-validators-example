/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.shared.models;

import de.knightsoftnet.gwtp.spring.shared.models.User;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * The <code>UserData</code> class implements contains the data of a user.
 *
 * @author Manfred Tremmel
 */
public class UserData implements User {

  /**
   * login name of the user.
   */
  private String userName;

  /**
   * password of the user.
   */
  private String password;

  /**
   * default constructor.
   */
  public UserData() {
    this(null);
  }

  /**
   * constructor initializing key field.
   *
   * @param userName user to set
   */
  public UserData(final String userName) {
    this(userName, null);
  }

  /**
   * constructor initializing all fields.
   *
   * @param userName user to set
   * @param password password to set
   */
  public UserData(final String userName, final String password) {
    super();
    this.userName = userName;
    this.password = password;
  }

  @Override
  public String getUserName() {
    return userName;
  }

  @Override
  public void setUserName(final String userName) {
    this.userName = userName;
  }

  @Override
  public boolean isLoggedIn() {
    return StringUtils.isNotEmpty(userName);
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(final String password) {
    this.password = password;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(userName);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final UserData other = (UserData) obj;
    return StringUtils.equals(userName, other.getUserName());
  }
}
