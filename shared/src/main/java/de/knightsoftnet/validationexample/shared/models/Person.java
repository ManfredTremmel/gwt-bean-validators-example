/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.shared.models;

import de.knightsoftnet.gwtp.spring.shared.data.jpa.domain.AbstractPersistable;
import de.knightsoftnet.validators.shared.PhoneNumberValue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.Size;

/**
 * person data.
 *
 * @author Manfred Tremmel
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@PhoneNumberValue(fieldCountryCode = "postalAddress.countryCode", fieldPhoneNumber = "phoneNumber",
    allowCommon = false, allowDin5008 = false, allowE123 = false, allowUri = false, allowMs = true)
public class Person extends AbstractPersistable<Long> {

  @NotNull
  @Enumerated(EnumType.STRING)
  private SalutationEnum salutation;
  @NotEmpty
  @Size(max = 50)
  private String firstName;
  @NotEmpty
  @Size(max = 50)
  private String lastName;
  @PastOrPresent
  private LocalDate birthday;

  private String phoneNumber;

  @ManyToOne(optional = false)
  @JoinColumn(name = "cost_center_id", nullable = false, updatable = true)
  @NotNull
  @Valid
  private CostCenterEntity costCenter;

  @ManyToOne(optional = true)
  @JoinColumn(name = "secondary_cost_center_id", nullable = true, updatable = true)
  @Valid
  private CostCenterEntity secondaryCostCenter;

  @NotNull
  @Valid
  private PostalAddress postalAddress;

  @OneToMany(mappedBy = "person", fetch = FetchType.EAGER, cascade = CascadeType.ALL,
      orphanRemoval = true)
  @NotNull
  @Valid
  @Size(min = 1)
  private final List<EmailEntity> emails;

  /**
   * default constructor.
   */
  public Person() {
    super();
    postalAddress = new PostalAddress();
    emails = new ArrayList<>();
    costCenter = new CostCenterEntity();
  }

  public SalutationEnum getSalutation() {
    return salutation;
  }

  public void setSalutation(final SalutationEnum salutation) {
    this.salutation = salutation;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public LocalDate getBirthday() {
    return birthday;
  }

  public void setBirthday(final LocalDate birthday) {
    this.birthday = birthday;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(final String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public CostCenterEntity getCostCenter() {
    return costCenter;
  }

  public void setCostCenter(final CostCenterEntity costCenter) {
    this.costCenter = costCenter;
  }

  public CostCenterEntity getSecondaryCostCenter() {
    return secondaryCostCenter;
  }

  public void setSecondaryCostCenter(final CostCenterEntity secondaryCostCenter) {
    this.secondaryCostCenter = secondaryCostCenter;
  }

  public PostalAddress getPostalAddress() {
    return postalAddress;
  }

  public void setPostalAddress(final PostalAddress postalAddress) {
    this.postalAddress = postalAddress;
  }

  public List<EmailEntity> getEmails() {
    emails.forEach(email -> email.setPerson(this));
    return emails;
  }

  /**
   * setter for email list.
   *
   * @param emails list of emails to set
   */
  public void setEmails(final List<EmailEntity> emails) {
    this.emails.clear();
    if (emails != null && emails.size() > 0) {
      emails.forEach(email -> email.setPerson(this));
      this.emails.addAll(emails);
    }
  }

  @Override
  public String toString() {
    return "Person [salutation=" + salutation + ", firstName=" + firstName + ", lastName="
        + lastName + ", birthday=" + birthday + ", phoneNumber=" + phoneNumber + ", costCenter="
        + costCenter + ", secondaryCostCenter=" + secondaryCostCenter + ", postalAddress="
        + postalAddress + ", emails=" + emails + "]";
  }
}
