package de.knightsoftnet.validationexample.shared.models;

import de.knightsoftnet.gwtp.spring.shared.data.jpa.domain.AbstractPersistable;
import de.knightsoftnet.validators.shared.Email;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

/**
 * email entity.
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailEntity extends AbstractPersistable<Long> {

  @Email
  @NotEmpty
  private String email;

  @NotNull
  @Enumerated(EnumType.STRING)
  private EmailTypeEnum type;

  @ManyToOne(optional = false, cascade = CascadeType.PERSIST)
  @JsonIgnore
  private Person person;

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getEmail() {
    return email;
  }

  public EmailTypeEnum getType() {
    return type;
  }

  public void setType(final EmailTypeEnum type) {
    this.type = type;
  }

  public Person getPerson() {
    return person;
  }

  public void setPerson(final Person person) {
    this.person = person;
  }

  @Override
  public String toString() {
    return "EmailEntity [email=" + email + ", type=" + type + "]";
  }

}
