package de.knightsoftnet.validationexample.shared.models;

import de.knightsoftnet.validators.shared.PostalCode;
import de.knightsoftnet.validators.shared.data.CountryEnum;

import jakarta.persistence.Embeddable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

/**
 * postal address jpa embeddable.
 */
@Embeddable
@PostalCode(fieldCountryCode = "countryCode", fieldPostalCode = "postalCode")
public class PostalAddress {

  /**
   * default length limit.
   */
  public static final int LENGTH_DEFAULT = 255;

  /**
   * length limit of the street name.
   */
  public static final int LENGTH_STREET_NUMBER = 10;

  /**
   * length limit postal code.
   */
  public static final int LENGTH_POSTAL_CODE = 10;

  /**
   * street name.
   */
  @Size(max = LENGTH_DEFAULT)
  @NotEmpty
  private String street;

  /**
   * street number.
   */
  @Size(max = LENGTH_STREET_NUMBER)
  @NotEmpty
  private String streetNumber;

  /**
   * postal code.
   */
  @Size(max = LENGTH_POSTAL_CODE)
  @NotEmpty
  private String postalCode;

  /**
   * localized name of the locality (city/town).
   */
  @Size(max = LENGTH_DEFAULT)
  @NotEmpty
  private String locality;

  /**
   * country code.
   */
  @NotNull
  @Enumerated(EnumType.STRING)
  private CountryEnum countryCode;

  /**
   * localized name of the region (state).
   */
  @Size(max = LENGTH_DEFAULT)
  private String region;

  public String getStreet() {
    return street;
  }

  public void setStreet(final String street) {
    this.street = street;
  }

  public String getStreetNumber() {
    return streetNumber;
  }

  public void setStreetNumber(final String streetNumber) {
    this.streetNumber = streetNumber;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(final String postalCode) {
    this.postalCode = postalCode;
  }

  public String getLocality() {
    return locality;
  }

  public void setLocality(final String locality) {
    this.locality = locality;
  }

  public CountryEnum getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(final CountryEnum countryCode) {
    this.countryCode = countryCode;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(final String region) {
    this.region = region;
  }

  @Override
  public String toString() {
    return "PostalAddress [street=" + street + ", streetNumber=" + streetNumber + ", postalCode="
        + postalCode + ", locality=" + locality + ", countryCode=" + countryCode + ", region="
        + region + "]";
  }
}
