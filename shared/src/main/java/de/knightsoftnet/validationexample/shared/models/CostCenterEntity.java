package de.knightsoftnet.validationexample.shared.models;

import de.knightsoftnet.gwtp.spring.shared.data.jpa.domain.AbstractPersistable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;

/**
 * cost center entity.
 */
@Entity
@Table(indexes = {@Index(name = "cost_center_number", columnList = "number", unique = true)})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CostCenterEntity extends AbstractPersistable<Long> {

  @NotEmpty
  private String number;
  @NotEmpty
  private String name;

  public String getNumber() {
    return number;
  }

  public void setNumber(final String number) {
    this.number = number;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "CostCenterEntity [number=" + number + ", name=" + name + "]";
  }
}
