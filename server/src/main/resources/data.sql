INSERT INTO cost_center_entity (id, number, name) VALUES (1, '100100', 'First Costcenter');
INSERT INTO cost_center_entity (id, number, name) VALUES (2, '100200', 'Second Costcenter');

INSERT INTO person (id, salutation, first_name, last_name, birthday, phone_number, street, street_number, postal_code, locality, country_code, region, cost_center_id) VALUES (1, 'MRS', 'Erika', 'Mustermann', '1970-07-13', '+49 (8151) 4711', 'Heidestrasse', '17', '51147', 'Köln', 'DE', null, 1);
INSERT INTO person (id, salutation, first_name, last_name, birthday, phone_number, street, street_number, postal_code, locality, country_code, region, cost_center_id) VALUES (2, 'MR', 'Max', 'Mustermann', '1976-02-01', '+49 (8151) 4712', 'Musterstrasse', '1', '12345', 'Musterhausen', 'DE', null, 2);

INSERT INTO email_entity (id, email, type, person_id) VALUES (1, 'erika.mustermann@test.de', 'HOME', 1);
INSERT INTO email_entity (id, email, type, person_id) VALUES (2, 'max.mustermann@test.de', 'HOME', 2);

ALTER SEQUENCE COST_CENTER_ENTITY_SEQ RESTART WITH 52;
ALTER SEQUENCE PERSON_SEQ RESTART WITH 52;
ALTER SEQUENCE EMAIL_ENTITY_SEQ RESTART WITH 52;
