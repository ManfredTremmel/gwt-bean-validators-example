/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.server.spring;

import de.knightsoftnet.validationexample.server.spring.CustomRewriteFilter.CustomRewriteFilterHints;

import org.springframework.aot.hint.RuntimeHints;
import org.springframework.aot.hint.RuntimeHintsRegistrar;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ImportRuntimeHints;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.tuckey.web.filters.urlrewrite.Conf;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;

import java.io.IOException;

import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;

@Component
@ImportRuntimeHints(CustomRewriteFilterHints.class)
public class CustomRewriteFilter extends UrlRewriteFilter {

  private static final String CONFIG_LOCATION = "classpath:/urlrewrite.xml";

  @Value(CONFIG_LOCATION)
  private Resource resource;

  @Override
  protected void loadUrlRewriter(final FilterConfig filterConfig) throws ServletException {
    try {
      final Conf conf = new Conf(filterConfig.getServletContext(), resource.getInputStream(),
          resource.getFilename(), "urlrewrite");
      checkConf(conf);
    } catch (final IOException ex) {
      throw new ServletException(
          "Unable to load URL rewrite configuration file from " + CONFIG_LOCATION, ex);
    }
  }

  static class CustomRewriteFilterHints implements RuntimeHintsRegistrar {
    @Override
    public void registerHints(final RuntimeHints hints, final ClassLoader classLoader) {
      hints.resources().registerPattern("urlrewrite.xml");
    }
  }
}
