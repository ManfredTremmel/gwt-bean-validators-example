/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.server.spring;

import de.knightsoftnet.gwtp.spring.server.security.AuthFailureHandler;
import de.knightsoftnet.gwtp.spring.server.security.AuthSuccessHandler;
import de.knightsoftnet.gwtp.spring.server.security.CsrfCookieHandler;
import de.knightsoftnet.gwtp.spring.server.security.HttpAuthenticationEntryPoint;
import de.knightsoftnet.gwtp.spring.server.security.HttpLogoutSuccessHandler;
import de.knightsoftnet.gwtp.spring.shared.ResourcePaths.User;
import de.knightsoftnet.validationexample.server.spring.WebSecurityConfig.WebSecurityConfigHints;
import de.knightsoftnet.validationexample.shared.AppResourcePaths;
import de.knightsoftnet.validationexample.shared.navigation.NameTokens;
import de.knightsoftnet.validators.shared.Parameters;
import de.knightsoftnet.validators.shared.ResourcePaths;

import org.springframework.aot.hint.RuntimeHints;
import org.springframework.aot.hint.RuntimeHintsRegistrar;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportRuntimeHints;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.password.LdapShaPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

import jakarta.inject.Inject;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * configuration for spring security.
 *
 * @author Manfred Tremmel
 */
@Configuration
@ImportRuntimeHints(WebSecurityConfigHints.class)
public class WebSecurityConfig {

  private static final String LOGIN_PATH = User.ROOT + User.LOGIN;

  private final HttpAuthenticationEntryPoint authenticationEntryPoint;
  private final AuthSuccessHandler authSuccessHandler;
  private final AuthFailureHandler authFailureHandler;
  private final HttpLogoutSuccessHandler logoutSuccessHandler;
  private final CsrfCookieHandler csrfCookieHandler;

  /**
   * constructor injecting needed resources.
   */
  @Inject
  public WebSecurityConfig(final HttpAuthenticationEntryPoint authenticationEntryPoint,
      final AuthSuccessHandler authSuccessHandler, final AuthFailureHandler authFailureHandler,
      final HttpLogoutSuccessHandler logoutSuccessHandler,
      final CsrfCookieHandler csrfCookieHandler) {
    super();
    this.authenticationEntryPoint = authenticationEntryPoint;
    this.authSuccessHandler = authSuccessHandler;
    this.authFailureHandler = authFailureHandler;
    this.logoutSuccessHandler = logoutSuccessHandler;
    this.csrfCookieHandler = csrfCookieHandler;
  }

  /**
   * configure security settings.
   */
  @Bean
  SecurityFilterChain configure(final HttpSecurity http) throws Exception { // NOPMD

    // csrf/xsrf protection
    http.csrf(csrfCustomizer -> csrfCustomizer.csrfTokenRepository(csrfTokenRepository())) //
        .addFilterAfter(csrfHeaderFilter(), CsrfFilter.class) //
        .authorizeHttpRequests(authorize -> authorize

            // services without authentication
            .requestMatchers("/", //
                "/index.html", //
                "/favicon.ico", //
                NameTokens.LOGIN, //
                NameTokens.LOGOUT, //
                NameTokens.SEPA, //
                NameTokens.ADDRESS, //
                NameTokens.EMAIL_LIST, //
                NameTokens.COST_CENTER, //
                NameTokens.COST_CENTER + "**", //
                NameTokens.PERSON, //
                NameTokens.PERSON + "**", //
                NameTokens.PHONE_NUMBER, //
                NameTokens.SECRET, //
                NameTokens.SETTINGS, //
                "/gwtBeanValidatorsExample/**", //

                AppResourcePaths.PHONE_NUMBER, //
                AppResourcePaths.BANK, //
                AppResourcePaths.POSTAL_ADDRESS, //
                AppResourcePaths.EMAIL_LIST, //
                AppResourcePaths.SEPA, //
                ResourcePaths.PhoneNumber.ROOT + "/**") //
            .permitAll() //

            // all other requests need authentication
            .anyRequest().authenticated() //

        // handle not allowed access, delegate to authentication entry point
        ) //
        .exceptionHandling(exceptionCustomizer -> exceptionCustomizer
            .authenticationEntryPoint(authenticationEntryPoint)) //

        // login form handling
        .formLogin(formLogin -> formLogin.permitAll() //
            .loginProcessingUrl(LOGIN_PATH) //
            .usernameParameter(Parameters.USERNAME) //
            .passwordParameter(Parameters.PASSWORD) //
            .successHandler(authSuccessHandler) //
            .failureHandler(authFailureHandler))

        .logout(logoutCustomizer -> logoutCustomizer.permitAll() //
            .logoutRequestMatcher(new AntPathRequestMatcher(LOGIN_PATH, "DELETE")) //
            .logoutSuccessHandler(logoutSuccessHandler))

        .sessionManagement(sessionCustomizer -> sessionCustomizer.maximumSessions(1));

    return http.build();
  }

  /**
   * include ldap authentication configuration.
   */
  @SuppressWarnings("deprecation")
  @Bean
  AuthenticationManager authenticationManager(final HttpSecurity http) throws Exception {
    final AuthenticationManagerBuilder auth =
        http.getSharedObject(AuthenticationManagerBuilder.class);
    auth.ldapAuthentication() //
        .userDnPatterns("uid={0},ou=people") //
        .groupSearchBase("ou=groups") //
        .contextSource() //
        .url("ldap://localhost:8389/dc=springframework,dc=org") //
        .and() //
        .passwordCompare() //
        .passwordEncoder(new LdapShaPasswordEncoder()) //
        .passwordAttribute("userPassword");
    return auth.build();
  }

  private Filter csrfHeaderFilter() {
    return new OncePerRequestFilter() {
      @Override
      protected void doFilterInternal(final HttpServletRequest request,
          final HttpServletResponse response, final FilterChain filterChain)
          throws ServletException, IOException {
        csrfCookieHandler.setCookie(request, response);
        filterChain.doFilter(request, response);
      }
    };
  }

  /**
   * define csrf header entry.
   *
   * @return csrf header entry
   */
  @Bean
  CsrfTokenRepository csrfTokenRepository() {
    final HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
    repository.setHeaderName(ResourcePaths.XSRF_HEADER);
    return repository;
  }

  /**
   * json serialization of Page interfaces should be done by PageImpl.
   */
  @Configuration
  @EnableSpringDataWebSupport(
      pageSerializationMode = EnableSpringDataWebSupport.PageSerializationMode.VIA_DTO)
  public class JacksonConfig {
  }

  static class WebSecurityConfigHints implements RuntimeHintsRegistrar {
    @Override
    public void registerHints(final RuntimeHints hints, final ClassLoader classLoader) {
      hints.resources().registerPattern("test-server.ldif");
    }
  }
}
